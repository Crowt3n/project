<?php
class Ticket {
      private $db;

      public function __construct(){
          $this->db = new Database;
      }

      public function create($data){
          $this->db->query('INSERT INTO tickets (event_id, total, remaining, price, name, description) VALUES (:event_id, :total, :remaining, :price, :name, :description);');
          $this->db->bind(':event_id', $data['event_id']);
          $this->db->bind(':total', $data['count']);
          $this->db->bind(':remaining', $data['count']);
          $this->db->bind(':price', $data['price']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);

          if($this->db->execute()){
              return true;
          } else {
              die('Ticket creation failed');
          }
      }

      public function getTicketsWhereIds($ids){
        $sql="SELECT * FROM tickets WHERE id in(".implode(',', $ids).");";
        $this->db->query($sql);

        $result = $this->db->resultSet();
        if ($this->db->rowCount() > 0) {
          return $result;
        } else {
          return false;
        }
      }

      public function getTicketById($id){
        $this->db->query('SELECT * FROM tickets where id=:id');
        $this->db->bind(':id', $id);

        $row = $this->db->single();
        if ($this->db->rowCount() > 0) {
          return $row;
        } else {
          return false;
        }
      }

      public function createdTickets($user_id){
        $this->db->query('SELECT events.id as event_id, events.name as event_name FROM events where events.user_id = :user_id and type=:type');
        $this->db->bind(':user_id', $user_id);
        $this->db->bind(':type', 'paid');

        $result = $this->db->resultSet();
        if ($this->db->rowCount() > 0) {
          return $result;
        } else {
          return false;
        }
      }

      public function reduceSeatsOpen($event_id, $seats_left){
        $this->db->query('UPDATE events SET seats_open=:seats_open WHERE id = :id;');
        $this->db->bind(':seats_open', $seats_left);
        $this->db->bind(':id', $event_id);

        if ($this->db->execute()) {
          return true;
        } else {
          die('Something went wrong.');
        }
      }

      public function getEventById($event_id){
        $this->db->query('SELECT * FROM events where id=:id');
        $this->db->bind(':id', $event_id);

        $row = $this->db->single();
        if ($this->db->rowCount() > 0) {
          return $row;
        } else {
          return false;
        }
      }

      public function update($data){
        $this->db->query('UPDATE tickets SET total=:total, remaining=:remaining, price=:price, name=:name, description=:description WHERE id=:id');
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':total', $data['count']);
        $this->db->bind(':remaining', $data['count']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':description', $data['description']);

        if ($this->db->execute()) {
          return true;
        } else {
          die('Something went wrong.');
        }
      }

      public function list($event_id){
        $this->db->query('SELECT * from tickets where tickets.event_id=:event_id;');
        $this->db->bind(':event_id', $event_id);

        $result = $this->db->resultSet();

        if ($this->db->rowCount() > 0) {
          return $result;
        } else {
          return false;
        }
      }

      public function updateTicket($ticket){
        $this->db->query('UPDATE tickets SET remaining=:remaining WHERE id=:id');
        $this->db->bind(':id', $ticket->id);
        $this->db->bind(':remaining', $ticket->remaining);

        if ($this->db->execute()) {
          return true;
        } else {
          die('Something went wrong.');
        }
      }

      public function purchasedTickets($user_id, $ticket_id, $quantity, $status, $address, $city, $country, $username, $event_id, $ticket_name){
        $this->db->query('INSERT INTO purchased_tickets (user_id, ticket_id, quantity, status, address, city, country, user_name, event_id, ticket_name)
        VALUES (:user_id, :ticket_id, :quantity, :status, :address, :city, :country, :username, :event_id, :ticket_name);');
        $this->db->bind(':user_id', $user_id);
        $this->db->bind(':ticket_id', $ticket_id);
        $this->db->bind(':quantity', $quantity);
        $this->db->bind(':status', $status);
        $this->db->bind(':address', $address);
        $this->db->bind(':city', $city);
        $this->db->bind(':country', $country);
        $this->db->bind(':username', $username);
        $this->db->bind(':event_id', $event_id);
        $this->db->bind(':ticket_name', $ticket_name);

        if($this->db->execute()){
            return true;
        } else {
            die('Insertion in purchased tickets failed');
        }
      }

      public function addAtendance($user_id, $event_id, $ticket_id){
        $this->db->query('INSERT INTO attendance (user_id, event_id, ticket_id) VALUES (:user_id, :event_id, :ticket_id);');
        $this->db->bind(':user_id', $user_id);
        $this->db->bind(':event_id', $event_id);
        $this->db->bind(':ticket_id', $ticket_id);

        if($this->db->execute()){
            return true;
        } else {
            die('Insertion in attendance failed');
        }
      }

      public function getPurchasedTickets($user_id){
        $this->db->query('SELECT * FROM purchased_tickets WHERE user_id = :user_id;');
        $this->db->bind(':user_id', $user_id);

        $result = $this->db->resultSet();

        if ($this->db->rowCount() > 0) {
          return $result;
        } else {
          return false;
        }
      }
}
?>
