<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
        <div class="col-md-9 col-sm-12 ml-auto mt-5">
          <?php if (!isset($data['no_tickets'])): ?>

          <div class="row">
            <div class="col-md-12 mx-auto m-5">
              <h2 class="text-center">Билети по събитие</h2>
            </div>
          </div>

          <?php foreach($data as $event) :?>
          <div class="row">
            <div class="col-md-5">
              <h5 class="text-center"><?php echo $event->event_name; ?></h5>
            </div>
            <div class="col-md-3">
              <a class="btn btn-success" href="<?php echo URLROOT;?>/tickets/create?id=<?php echo $event->event_id;?>">Създай билет</a>
            </div>
            <div class="col-md-4">
              <a class="btn btn-primary" href="<?php echo URLROOT;?>/tickets/list?id=<?php echo $event->event_id;?>">Всички билети за това събитие</a>
            </div>

          </div>
          <hr>
        <?php endforeach; ?>
      <?php else: ?>
        <div class="row mt-5">
          <div class="col-md-8">
            <h4 class="text-center">Не сте създали събития.</h4>
          </div>
          <div class="col-md-4">
            <a href="<?php echo URLROOT; ?>/events/create" class="btn btn-primary">Създай събитие</a>
          </div>
        </div>
    <?php endif; ?>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
