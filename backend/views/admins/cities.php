<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">

  <div class="row">

    <div class="col">
      <div class="row">
        <p class="display-4 mt-4">Редактиране на град</p>
      </div>
      <div class="row">
        <select id="city" class="form-control" name="cit_choice">
          <?php foreach ($data['cities'] as $city): ?>
            <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="row">
        <form class="" action="<?php URLROOT; ?>/admins/editCities" method="post">
          <div class="form-group">
            <input type="hidden" name="city_id" value="">
          </div>
          <div class="form-group">
            <label for="city_name">Име на град</label>
            <input class="form-control" type="text" name="city_name" value="">
          </div>
          <div class="form-group">
            <input class="btn btn-success" type="submit" name="submit" value="Обнови">
          </div>
        </form>
      </div>
    </div>
    <div class="col">
      <div class="row">
        <p class="display-4 mt-4">Добавяне на град</p>
      </div>
      <form class="" action="<?php URLROOT; ?>/admins/addCity" method="post">
        <div class="form-group">
          <label for="name">Име на град</label>
          <input class="form-control" type="text" name="name">
        </div>
        <div class="form-group">
          <input class="btn btn-primary" type="submit" name="submit" value="Добави">
        </div>
      </form>
    </div>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
<script type="text/javascript">
var id,name;
  $(document).ready(function(){

    $('#city').change(function(){
                  var citid = $("#city").val();
                  $.ajax({
                    type:'POST',
                    url: 'http://localhost/admins/getCity',
                    data:{city_id: citid},
                    dataType: 'json',
                    success: function(data, string, jqXHR){
                    data = $.parseJSON(JSON.stringify(data));

                    id = data.id;
                    name=data.name;
                    desc=data.description;
                    $("input[name*='city_id']").val(id);
                    $("input[name*='city_name']").val(name);
                    }
                  });
      });
    });


</script>
