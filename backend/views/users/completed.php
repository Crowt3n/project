<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row">
      <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
      <div class="col-md-9 col-sm-12 ml-auto mt-5">
        <div class="row mt-5 mb-5">
          <div class="col">
              <h1 class="text-center">Приключени поръчки</h1>
          </div>
        </div>
        <?php if (empty($data['no_orders'])): ?>
          <div class="row">
            <div class="col-md-2">
              <p class="">Получател</p>
            </div>
            <div class="col-md-2">
              <p class="">Име на билет</p>
            </div>
            <div class="col-md-1">
              <p class="">Кол.</p>
            </div>
            <div class="col-md-3">
             <p class="">Адрес за доставка</p>
            </div>
            <div class="col-md-2">
              <p class="">Град</p>
            </div>
            <div class="col-md-2">
              <p>Статус</p>
            </div>
          </div>
            <hr>  <hr>
          <?php foreach ($data['orders'] as $order): ?>
          <div class="row">
            <div class="col-md-2">
              <?php echo $order->user_name; ?>
            </div>
            <div class="col-md-2">
              <?php echo $order->ticket_name; ?>
            </div>
            <div class="col-md-1">
              <?php echo $order->quantity; ?>
            </div>
            <div class="col-md-3">
              <?php echo $order->address; ?>
            </div>
            <div class="col-md-2">
              <?php echo $order->city; ?>
            </div>
            <div class="col-md-2">
              <?php echo $order->status; ?>
            </div>

          </div>
          <hr>
          <?php endforeach; ?>
          <?php else: ?>
            <div class="row">
                <div class="col">
                  <p class="text-center"><?php echo $data['no_orders']; ?></p>
                </div>
            </div>
        <?php endif; ?>

      </div>
  </div>

</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
