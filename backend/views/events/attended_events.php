<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
        <div class="col-md-9 col-sm-12 ml-auto mt-5">
          <?php if (!empty($data['events'])): ?>

          <h2 class="text-center">Събития на които ще присъствате</h2>

          <div class="row mt-5 mb-5">
            <div class="col-md-3">
              <p class="text-center">Име</p>
            </div>
            <div class="col-md-3">
              <p class="text-center">Кратко описание</p>
            </div>
            <div class="col-md-2">
              <p class="text-center">Стартира на</p>
            </div>
            <div class="col-md-2">
              <p class="text-center">Приключва на</p>
            </div>
            <div class="col-md-2">

            </div>
          </div>
          <hr><hr>
          <?php foreach ($data['events'] as $event): ?>
            <div class="row">
              <div class="col-md-3">
                <p class="text-center"><?php echo $event->name; ?></p>
              </div>
              <div class="col-md-3">
                <p class="text-center"><?php echo $event->short_desc; ?></p>
              </div>
              <div class="col-md-2">
                <p class="text-center"><?php echo $event->date_start; ?></p>
              </div>
              <div class="col-md-2">
                <p class="text-center"><?php echo $event->date_end; ?></p>
              </div>
              <div class="col-md-2">
                <a class="btn btn-primary" href="<?php echo URLROOT; ?>/events/display?id=<?php echo $event->id; ?>">Виж събитието</a>
              </div>
            </div>
            <hr>
          <?php endforeach; ?>
        <?php else: ?>
          <div class="row mt-5">
            <div class="col-md-12">
              <h4 class="text-center">Все още не сте закупили билети.</h4>
            </div>

          </div>

        <?php endif; ?>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
