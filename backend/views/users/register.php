<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
                <h2 class="text-center mt-2 mb-5">Създаване на профил</h2>

            <form action="<?php echo URLROOT;?>/users/register" method="post">
                <div class="form-group">
                    <label for="first_name">Име: <sup>*</sup></label>
                    <input type="text" name="first_name" class="form-control form-control-lg <?php echo (!empty($data['first_name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['first_name'];?>">
                    <span class="invalid-feedback"><?php echo $data['first_name_err'];?></span>
                </div>

                <div class="form-group">
                    <label for="last_name">Фамилия: <sup>*</sup></label>
                    <input type="text" name="last_name" class="form-control form-control-lg <?php echo (!empty($data['last_name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['last_name'];?>">
                    <span class="invalid-feedback"><?php echo $data['last_name_err'];?></span>
                </div>

                <div class="form-group">
                    <label for="email">Мейл: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['email'];?>">
                    <span class="invalid-feedback"><?php echo $data['email_err'];?></span>
                </div>

                <div class="form-group">
                    <label for="password">Парoла: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['password'];?>">
                    <span class="invalid-feedback"><?php echo $data['password_err'];?></span>
                </div>

                <div class="form-group">
                    <label for="confirm_password">Потвърди парола: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control form-control-lg <?php echo (!empty($data['confirm_password_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['confirm_password'];?>">
                    <span class="invalid-feedback"><?php echo $data['confirm_password_err'];?></span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Регистрирай се" class="btn btn-success btn-block">
                    </div>
                    <div class="col">
                        <a href="<?php echo URLROOT;?>/users/login" class="btn btn-light btn-block">Имате профил? Вход</a>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
