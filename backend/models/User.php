<?php
    class User{
        private $db;

        public function __construct(){
            $this->db = new Database;
        }

        public function findUserByEmail($email){
            $this->db->query('SELECT * FROM users WHERE email = :email;');
            $this->db->bind(':email', $email);

            $row = $this->db->single();

            if($this->db->rowCount() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function register($data){
            $this->db->query('INSERT INTO users (first_name, last_name, email, password) VALUES (:first_name, :last_name, :email, :password)');
            $this->db->bind(':first_name', $data['first_name']);
            $this->db->bind(':last_name', $data['last_name']);
            $this->db->bind(':email', $data['email']);
            $this->db->bind(':password', $data['password']);

            if($this->db->execute()){
                redirect('users/login');
            } else {
                die('Registration failed.');
            }
        }

        public function login($email, $password){
            $this->db->query('SELECT * FROM users WHERE email = :email');
            $this->db->bind(':email', $email);

            $row = $this->db->single();

            $hashed_password = $row->password;
            if(password_verify($password, $hashed_password)){
                return $row;
            } else {
                return false;
            }
        }

        public function loadCurrentUser(){
            $this->db->query('SELECT * FROM users WHERE id = :id');
            $this->db->bind(':id', $_SESSION['user_id']);

            $row = $this->db->single();

            if($this->db->rowCount() > 0){
                return $row;
            } else {
                return false;
            }
        }

        public function updateCurrentUser($data){
          $this->db->query('UPDATE users SET first_name = :first_name, last_name=:last_name, password=:password WHERE id=:id;');
          $this->db->bind(':first_name', $data['first_name']);
          $this->db->bind(':last_name', $data['last_name']);
          $this->db->bind(':password', $data['password']);
          $this->db->bind(':id', $_SESSION['user_id']);

          if($this->db->execute()){
              return true;
          } else {
              return false;
          }
        }

        public function getEventsByUserId($user_id){
          $this->db->query('SELECT id FROM events WHERE user_id=:user_id;');
          $this->db->bind(':user_id', $user_id);

          $result = $this->db->resultSet();
          if ($this->db->rowCount() > 0) {
            return $result;
          } else {
            return false;
          }
        }

        public function notSentTickets($event_ids){
          $sql="SELECT * FROM purchased_tickets WHERE event_id in(".implode(',', $event_ids).") and status = 'Готов за изпращане';";
          $this->db->query($sql);

          $result = $this->db->resultSet();
          if ($this->db->rowCount() > 0) {
            return $result;
          } else {
            return false;
          }
        }

        public function orderSentUpdate($id, $status){
          $this->db->query("UPDATE purchased_tickets SET status=:status WHERE id=:id");
          $this->db->bind(':id', $id);
          $this->db->bind(':status', $status);

          if ($this->db->execute()) {
            return true;
          } else {
            die('Order update failed');
          }
        }

        public function completedOrders($event_ids){
          $sql="SELECT * FROM purchased_tickets WHERE event_id in(".implode(',', $event_ids).") and status = 'Изпратен';";
          $this->db->query($sql);

          $result = $this->db->resultSet();
          if ($this->db->rowCount() > 0) {
            return $result;
          } else {
            return false;
          }
        }
    }

?>
