<?php 

    /*
    Base controller 
    Loads the models and views
    */
    class Controller {

        //Load model
        public function model($model){
            require_once '../backend/models/' . $model . '.php';
            return new $model();
        }

        //Load view 
        public function view($view, $data = []){
            if(file_exists('../backend/views/' . $view . '.php'))
            {
                require_once '../backend/views/' . $view . '.php';
            } else {
                //View does not exist
                die('View does not exist');
            }
        }
    }