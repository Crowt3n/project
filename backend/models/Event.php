<?php

class Event{

  private $db;


  public function __construct(){
      $this->db = new Database;
  }

  public function getAllCities(){
    $this->db->query('SELECT * FROM cities');
    $result = $this->db->resultSet();

    if ($this->db->rowCount()>0) {
      return $result;
    } else {
      return false;
    }
  }

  public function getEventsByCity($city){
    $this->db->query('SELECT * FROM events WHERE city = :city;');
    $this->db->bind(':city', $city);

    $events = $this->db->resultSet();

    if($this->db->rowCount() > 0){
        return $events;
    } else {
        return false;
    }
  }

  public function findEventByName($name){
      $this->db->query('SELECT * FROM events WHERE name = :name;');
      $this->db->bind(':name', $name);

      $row = $this->db->single();

      if($this->db->rowCount() > 0){
          return true;
      } else {
          return false;
      }
  }

  public function getEventById($id){
    $this->db->query('SELECT * FROM events WHERE id = :id;');
    $this->db->bind(':id', $id);

    $row = $this->db->single();

    if($this->db->rowCount() > 0){
        return $row;
    } else {
        return false;
    }
  }

  public function create($data){
      $this->db->query('INSERT INTO events (user_id, name, short_desc, description, address, address_note, images_url, seats, seats_open, type, date_start, date_end, city)
                        VALUES (:user_id, :name, :short_desc, :description, :address, :address_note, :images_url, :seats, :seats_open, :type, :date_start, :date_end, :city)');
      $this->db->bind(':user_id', $data['user_id']);
      $this->db->bind(':name', $data['name']);
      $this->db->bind(':short_desc', $data['short_description']);
      $this->db->bind(':description', $data['description']);
      $this->db->bind(':address', $data['address']);
      $this->db->bind(':address_note', $data['address_note']);
      $this->db->bind(':images_url', $data['main_picture']);
      $this->db->bind(':seats', $data['seats']);
      $this->db->bind(':seats_open', $data['seats']);
      $this->db->bind(':type', $data['type']);
      $this->db->bind(':date_start', $data['date_start']);
      $this->db->bind(':date_end', $data['date_end']);
      $this->db->bind(':city', $data['city']);

      if($this->db->execute()){
          return true;
      } else {
          die('Events creation failed');
      }
  }

  public function getLastCreatedEvent($user_id){
    $this->db->query('SELECT * FROM events WHERE user_id=:user_id ORDER BY id DESC limit 1;');
    $this->db->bind(':user_id', $user_id);

    $event = $this->db->single();
    if($this->db->rowCount() > 0){
        return $event;
    } else {
        return false;
    }
  }

  public function createdEvents($user_id){
    $this->db->query('SELECT * FROM events where user_id = :user_id order by id desc;');
    $this->db->bind(':user_id', $user_id);

    $events = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $events;
    } else {
      return false;
    }
  }

  public function getAllCategories(){
    $this->db->query('SELECT * FROM categories;');

    $categories = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $categories;
    } else {
      return false;
    }
  }

  public function update($data){
    $this->db->query('UPDATE events SET name=:name, short_desc=:short_desc, description=:description, address=:address, address_note=:address_note, seats=:seats, type=:type, date_start=:date_start, date_end=:date_end WHERE id = :id;');
    $this->db->bind(':id', $data['id']);
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':short_desc', $data['short_description']);
    $this->db->bind(':description', $data['description']);
    $this->db->bind(':address', $data['address']);
    $this->db->bind(':address_note', $data['address_note']);
    $this->db->bind(':seats', $data['seats']);
    $this->db->bind(':type', $data['type']);
    $this->db->bind(':date_start', $data['date_start']);
    $this->db->bind(':date_end', $data['date_end']);

    if($this->db->execute()){
        return true;
    } else {
        die('Events update failed');
    }
  }

  public function linkToCategory($event, $category){
    $this->db->query('INSERT INTO events2categories (event_id, category_id) values(:event_id, :category_id);');
    $this->db->bind(':event_id', $event->id);
    $this->db->bind(':category_id', $category);

    if($this->db->execute()){
        return true;
    } else {
        die('Linking failed.');
    }
  }

  public function getAttendedEvents($user_id){
    $this->db->query('SELECT * FROM attendance WHERE user_id=:user_id;');
    $this->db->bind(':user_id', $user_id);

    $result = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $result;
    } else {
      return false;
    }

  }

  public function getEventsWhereIdIn($event_id_array){
    $sql="SELECT * FROM events WHERE id in(".implode(',', $event_id_array).");";
    $this->db->query($sql);

    $result = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $result;
    } else {
      return false;
    }
  }

  public function getCategoryByEvent($event_id){
    $this->db->query('SELECT category_id FROM events2categories WHERE event_id = :event_id;');
    $this->db->bind(':event_id', $event_id);

    $cat_id = $this->db->single();
    if($this->db->rowCount() > 0){
        return $cat_id;
    } else {
        return false;
    }
  }

  public function getRelatedEvents($category_id, $event_id){
    $this->db->query('SELECT event_id FROM events2categories WHERE category_id=:cat_id and event_id != :event_id ORDER BY id DESC limit 3;');
    $this->db->bind(':cat_id', $category_id);
    $this->db->bind(':event_id', $event_id);

    $result = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $result;
    } else {
      return false;
    }
  }

  public function getLastTwentyEvents(){
    $this->db->query('SELECT * FROM events ORDER BY id DESC LIMIT 20;');

    $events = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $events;
    } else {
      return false;
    }
  }

  public function getEventsByCategory($cat_id){
    $this->db->query('SELECT event_id FROM events2categories WHERE category_id = :category_id;');
    $this->db->bind(':category_id', $cat_id);

    $event_ids = $this->db->resultSet();
    if($this->db->rowCount() > 0){
        return $event_ids;
    } else {
        return false;
    }
  }

  public function getEventsByDate($from, $to){
    $this->db->query('SELECT * FROM events where date_start >= :start and date_end <= :to order by id desc;');
    $this->db->bind(':start', $from);
    $this->db->bind(':to', $to);

    $events = $this->db->resultSet();

    if ($this->db->rowCount() > 0) {
      return $events;
    } else {
      return false;
    }
  }
}

 ?>
