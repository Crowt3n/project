<?php

class Events extends Controller {


  public function __construct(){
      $this->eventModel=$this->model('Event');
  }

    public function index(){
        $data = [];

        $this->view('events/index', $data);
    }

    public function create(){

        if(!isset($_SESSION['user_id'])){
          redirect('users/login');
        }

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //validate event form
            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'name' => $_POST['name'],
                'short_description' => $_POST['short_description'],
                'description' => $_POST['description'],
                'type' => strtolower($_POST['type']),
                'address' => $_POST['address'],
                'address_note' => $_POST['address_note'],
                'user_id' => $_SESSION['user_id'],
                'seats' => $_POST['seats'],
                'categories' => $this->eventModel->getAllCategories(),
                'category' => $_POST['category'],
                'date_start' => $_POST['date_start'],
                'date_end' => $_POST['date_end'],
                'cities' => $this->eventModel->getAllCities(),
                'city' => $_POST['city'],

                'name_err' => '',
                'short_description_err' => '',
                'description_err' => '',
                'type_err' => '',
                'address_err' => '',
                'address_note_err' => '',
                'main_picture_err' => '',
                'seats_err' => '',
                'date_start_err' => '',
                'date_end_err' => '',
            ];

            if(empty($data['name'])){
              $data['name_err'] = 'Моля попълнете името на събитието';
            } else {
              if($this->eventModel->findEventByName($data['name'])){
                $data['name_err'] = 'Събитие с това име вече съществува';
              }
            }
            if(empty($data['seats'])){
              $data['seats_err'] = 'Моля въведете броят места за Вашето събитие';
            }
            if(empty($data['short_description'])){
              $data['short_description_err'] = 'Моля добавете кратко описание';
            }
            if(empty($data['description'])){
              $data['description_err'] = 'Моля добавете описание на събитието';
            }
            if($data['type'] != 'free' && $data['type'] != 'paid'){
              $data['type_err'] = 'Непознат тип на събитие.';
            }
            if (empty($data['address'])) {
              $data['address_err'] = 'Моля добавете адрес на събитието';
            }
            if(empty($data['date_start'])){
              $data['date_start_err'] = 'Моля добавете дата на стартиране';
            }
            if($data['date_end'] < $data['start_date']){
              $data['date_end_err'] = 'Датата на приключване не може да бъде преди стартиращата.';
            }
            //process images only if there are no other errors
            if ($data['name_err'] == '' && $data['short_description_err'] == '' && $data['description_err'] == '' && $data['type_err'] == '' && $data['address_err'] == '' && $data['address_note_err'] == '')
            {
                if (!empty($_FILES)) {
                    //creates a folder for the event pictures
                    $image_folder = base64_encode($data['name']);

                    if (!is_dir('/var/www/html/frontend/images/' . $image_folder)) {
                        mkdir('/var/www/html/frontend/images/' . $image_folder);
                        $ext = str_replace("image/", ".", $_FILES['main_picture']['type']);
                        move_uploaded_file($_FILES['main_picture']['tmp_name'], '/var/www/html/frontend/images/' . $image_folder . '/main_image'.$ext);
                    } else {
                      //avoiding folder duplication
                      $image_folder .= 'a';
                      mkdir('/var/www/html/frontend/images/'. $image_folder);
                      $ext = str_replace("image/", ".", $_FILES['main_picture']['type']);
                      move_uploaded_file($_FILES['main_picture']['tmp_name'], '/var/www/html/frontend/images/'. $image_folder . '/main_image'.$ext);
                    }

                    $data['main_picture'] = "'images/' . $image_folder . '/main_image'";
                    $data['main_picture'] = str_replace("'", "", $data['main_picture']);
                    $data['main_picture'] = str_replace(" ", "", $data['main_picture']);
                    $data['main_picture'] = str_replace(".", "", $data['main_picture']);
                    $data['main_picture'] = $data['main_picture'] . $ext;
                    //process gallery
                    if (is_dir('/var/www/html/frontend/images/'. $image_folder)) {
                        mkdir('/var/www/html/frontend/images/' . $image_folder . '/gallery');
                        $cnt = 0;

                        foreach ($_FILES['gallery'] as $key => $all) {
                          foreach($all as $i => $val){
                            $picture[$i][$key] = $val;
                          }
                        }
                        foreach ($picture as $pic) {
                          $cnt++;
                          move_uploaded_file($pic['tmp_name'], '/var/www/html/frontend/images/' . $image_folder . '/gallery/' . $cnt .str_replace("image/", ".", $pic['type']));
                        }
                    }
                } else {
                  $data['main_picture_err'] = 'Моля добавете главно изображение';
                }
            }

            if ($data['name_err'] == ''
            && $data['short_description_err'] == ''
            && $data['description_err'] == ''
            && $data['type_err'] == ''
            && $data['address_err'] == ''
            && $data['address_note_err'] == ''
            && $data['main_picture_err'] == ''
            && $data['date_start_err'] == ''
            && $data['date_end_err'] == '')
            {
              //no errors, proceed with event creation
              if ($this->eventModel->create($data)) {
                $event = $this->eventModel->getLastCreatedEvent($_SESSION['user_id']);
                $this->eventModel->linkToCategory($event, $data['category']);
                if($data['type'] == 'paid'){
                  flash('event_success','Вашето събитие е създадено успешно');
                  $data['event_id'] = $event->id;
                  $data['event_name'] = $event->name;
                  $data['seats_open'] = $event->seats_open;
                  $this->view('tickets/create', $data);
                } else {
                  flash('event_success','Вашето събитие е създадено успешно');
                  redirect('events/createdEvents');
                }
              } else {
                die('Създаването на събитие се провали');
              }
            } else {
              $this->view('events/create', $data);
            }
        }
        else
        {
            $data = [
                'name' => '',
                'short_description' => '',
                'description' => '',
                'type' => '',
                'address' => '',
                'address_note' => '',
                'main_picture' => '',
                'gallery' => '',
                'seats' => '',
                'categories' => $this->eventModel->getAllCategories(),
                'date_start' => '',
                'date_end' => '',
                'cities' => $this->eventModel->getAllCities(),
            ];
            $this->view('events/create', $data);
        }
    }

    public function edit(){
      if(!isset($_SESSION['user_id'])){
        redirect('users/login');
      }

      if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])){
          //validate event form
          $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

          $data = [
              'name' => $_POST['name'],
              'short_description' => $_POST['short_description'],
              'description' => $_POST['description'],
              'type' => strtolower($_POST['type']),
              'address' => $_POST['address'],
              'address_note' => $_POST['address_note'],
              'user_id' => $_SESSION['user_id'],
              'seats' => $_POST['seats'],
              'category' => $_POST['category'],
              'date_start' => $_POST['date_start'],
              'date_end' => $_POST['date_end'],

              'name_err' => '',
              'short_description_err' => '',
              'description_err' => '',
              'type_err' => '',
              'address_err' => '',
              'address_note_err' => '',
              'main_picture_err' => '',
              'seats_err' => '',
              'date_start_err' => '',
              'date_end_err' => '',
          ];

          if(empty($data['name'])){
            $data['name_err'] = 'Моля попълнете име на събитието';
          } else {
            if($this->eventModel->findEventByName($data['name'])){
              if ($data['name'] != $_COOKIE['event_name']) {
                $data['name_err'] = 'Събитие с такова име вече съществува';
              }
            }
          }
          if(empty($data['seats'])){
            $data['seats_err'] = 'Моля добавете броя места за Вашето събитие';
          }
          if(empty($data['short_description'])){
            $data['short_description_err'] = 'Моля добавете кратко описание';
          }
          if(empty($data['description'])){
            $data['description_err'] = 'Моля добавете описание';
          }
          if($data['type'] != 'free' && $data['type'] != 'paid'){
            $data['type_err'] = 'Непознат тип на събитие';
          }
          if (empty($data['address'])) {
            $data['address_err'] = 'Моля добавете адрес на събитието';
          }
          if(empty($data['date_start'])){
            $data['date_start_err'] = 'Моля добавете дата на стартиране';
          }
          if($data['date_end'] < $data['start_date']){
            $data['date_end_err'] = 'Датата на приключване не може да бъде преди датата на стартиране';
          }

          if ($data['name_err'] == ''
          && $data['short_description_err'] == ''
          && $data['description_err'] == ''
          && $data['type_err'] == ''
          && $data['address_err'] == ''
          && $data['address_note_err'] == ''
          && $data['main_picture_err'] == ''
          && $data['date_start_err'] == ''
          && $data['date_end_err'] == '')
          {
            //no errors, proceed with event creation
            if ($this->eventModel->update($data)) {
              flash('register_success','Вашето събитие е обновено успешно');
              redirect('events/createdEvents');
            } else {
              die('Event update failed.');
            }
          } else {
            $this->view('events/edit', $data);
          }
      }
      else if ($_SERVER['REQUEST_METHOD'] = 'GET' && isset($_GET['id']))
      {
        if ($event = $this->eventModel->getEventById($_GET['id'])) {
          $data = [
            'name' => $event->name,
            'short_description' => $event->short_desc,
            'description' => $event->description,
            'type' => $event->type,
            'address' => $event->address,
            'address_note' => $event->address_note,
            'main_picture' => '',
            'gallery' => array(),
            'seats' => $event->seats,
            'date_start' => $event->date_start,
            'date_end' => $event->date_end,
          ];
          setcookie("event_name", $event->name);

          $this->view('events/edit', $data);
        }

      } else {

        $data = [
            'name' => '',
            'short_description' => '',
            'description' => '',
            'type' => '',
            'address' => '',
            'address_note' => '',
            'main_picture' => '',
            'gallery' => array(),
            'seats' => '',
        ];
        $this->view('events/edit', $data);
      }
    }

    public function display(){
      if ($_SERVER['REQUEST_METHOD'] = 'GET' && isset($_GET['id'])){
          if ($event = $this->eventModel->getEventById($_GET['id'])) {
            $data = [
              'id' => $event->id,
              'name' => $event->name,
              'type' => $event->type,
              'short_description' => $event->short_desc,
              'description' => $event->description,
              'address' => $event->address,
              'address_note' => $event->address_note,
              'seats' => $event->seats,
              'main_image' => $event->images_url,
              'date_start' => $event->date_start,
              'date_end' => $event->date_end,
              'city' => $event->city,
            ];
            $pics = array();
            $gallery = substr($data['main_image'], 0, strpos( $data['main_image'], '/main_image')).'/gallery';
            if (is_dir('/var/www/html/frontend/' .$gallery)) {
              $pics = array_slice(scandir('/var/www/html/frontend/' . $gallery, 0), 2);
              foreach($pics as $pic){
                $data['gallery'][] .=  $gallery . '/' . $pic;
              }
            }
            if ($events_you_may_like = $this->eventsYouMayLike($event->id)) {
              $data['events_you_may_like'] = $events_you_may_like;
            }

            $this->view('events/display', $data);
          }
      } else {
        redirect('events/index');
      }
    }

    public function eventsYouMayLike($event_id){

      if ($cat = $this->eventModel->getCategoryByEvent($event_id)) {
        if($event_ids = $this->eventModel->getRelatedEvents($cat->category_id, $event_id)){
          foreach($event_ids as $event){
            $ids[] = $event->event_id;
          }
          $events = $this->eventModel->getEventsWhereIdIn($ids);
          return $events;
        }
      }
      return false;
    }


    public function categories(){
      $categories = $this->eventModel->getAllCategories();
      $cities = $this->eventModel->getAllCities();
      $data['cities']=$cities;
      if (isset($_GET['cat_id'])) {
        $events = $this->eventModel->getEventsByCategory($_GET['cat_id']);
        $event_ids = array();
        if(!empty($events)){
        foreach($events as $event){
          if (!in_array($event->event_id, $event_ids)) {
            $event_ids[] = $event->event_id;
          }
        }

          $events = $this->eventModel->getEventsWhereIdIn($event_ids);
          $data['events'] = $events;
        } else {
          $data['no_events'] = 'Няма събития от тази категория';
        }

      } else if (isset($_POST['from']) || isset($_POST['to'])){
        if (!empty($_POST['to'])) {
          $to = $_POST['to'];
        } else {
          $to = date("Y-m-d", strtotime(date( "Y-m-d", strtotime(date("Y-m-d")))));
        }
        if(!empty($_POST['from'])){
          $from = $_POST['from'];
        } else {
          $from = date("Y-m-d", strtotime(date( "Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));
        }
        $events = $this->eventModel->getEventsByDate($from, $to);
        if($events){
          $data['events'] = $events;
        }else {
          $data['no_events'] = 'Няма събития от тази дата.';
        }
      } else if(isset($_POST['city_name'])){

        if($events = $this->eventModel->getEventsByCity($_POST['city_name'])){
          $data['events'] = $events;
        } else {
          $data['no_events'] = 'Няма събития от тази област.';
        }
      } else {
        $events = $this->eventModel->getLastTwentyEvents();
        $data['events'] = $events;
      }
      $data['categories'] = $categories;
      $this->view('events/categories', $data);
    }

    public function attendedEvents(){
      if (!isset($_SESSION['user_id'])) {
        redirect('users/login');
      }
      $att_events = array();
      $attendance = $this->eventModel->getAttendedEvents($_SESSION['user_id']);
      if (!empty($attendance)) {
        foreach ($attendance as $event) {
          if (!in_array($event->event_id, $att_events)) {
            $att_events[] = $event->event_id;
          }
        }

        $events = $this->eventModel->getEventsWhereIdIn($att_events);

        if (!$events) {
          $data['no_events'] = "Все още не сте закупили билети за събития.";
        } else {
          $data['events'] = $events;
        }
      }

      $this->view('events/attended_events', $data);
    }

    public function createdEvents(){
      if(!isset($_SESSION['user_id'])){
        redirect('users/login');
      }
      if($data = $this->eventModel->createdEvents($_SESSION['user_id'])){
        $this->view('events/created_events', $data);
      } else {
        $data['no_events'] = 'Все още не сте създали събитие.';
        $this->view('events/created_events', $data);
      }
    }
}
