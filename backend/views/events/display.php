<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
      <div class="col-md-12 mt-5">
        <h1 class="text-center"><?php echo $data['name'];?></h1>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-md-7 ml-auto">
        <img src="<?php echo URLROOT . '/' .$data['main_image'];?>" alt="Event image" class="img-fluid" style="max-height:60vh;">
      </div>

      <div class="col-md-5 ml-auto">
        <?php if(!$event->is_approved): ?>
        <div class="row">
          <div class="col mb-3 text-center">
            <span class="alert alert-danger">Събитието все още не е потвърдено!</span>
          </div>
        </div>
        <?php endif; ?>
          <div class="row">
            <div class="col-md-12 mx-auto">
              <p>Кратко описание: <?php echo $data['short_description'];?></p>
            </div>
          </div>

          <div class="row mt-5">
            <div class="col-md-12 mx-auto">
              <p>Стартира на: <?php echo str_replace('T', ' ', $data['date_start']); ?></p>
            </div>
          </div>

          <div class="row mt-5">
            <div class="col-md-12 mx-auto">
              <p>Приключва на: <?php echo str_replace('T', ' ', $data['date_end']); ?></p>
            </div>
          </div>

          <div class="row">
            <?php if($data['type'] == 'free') : ?>
              <!-- <button id="interested" type="button" name="button" class="btn btn-primary">Im interested</button> -->
            <?php else: ?>
              <div class="col">
                <a href="<?php echo URLROOT; ?>/tickets/buy?id=<?php echo $data['id']; ?>" class="btn btn-primary">Купи билет</a>
              </div>
            <?php endif; ?>


              <?php if (isset($_SESSION['admin_id'])): ?>
                <div class="col">
                  <form action="<?php echo URLROOT; ?>/admins/needsApproval" method="post">
                    <div class="form-group">
                      <input type="hidden" name="event_id" value="<?php echo $data['id']; ?>">
                      <input class="btn btn-success" type="submit" name="submit" value="Потвърди събитие">
                    </div>
                  </form>
                </div>
              <?php endif; ?>
            </div>

          </div>
      </div>
    <div class="row mt-5">
        <div class="col-md-12 mx-auto">
          <p>Описание: <?php echo $data['description']; ?></p>
        </div>

    </div>
    <hr>
    <div class="row">
    <?php   if(!empty($data['gallery'])): ?>
        <?php foreach($data['gallery'] as $pic) :?>
          <div class="col">
            <img src="<?php echo URLROOT . '/' . $pic; ?>" alt="gallery" class="img-thumbnail">
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-3 col-sm-12">
        <div class="row">
          <p>Тип на събитието</p>
        </div>
        <div class="row">
          <p><?php echo ($data['type'] == 'paid') ?  'Платено' : 'Безплатно'; ?></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="row">
          <p>Адрес</p>
        </div>
        <div class="row">
          <p><?php echo $data['address']; ?></p>
        </div>
      </div>
      <div class="col-md-3 col-sm-12">
        <div class="row">
          <p>Бележка за адрес</p>
        </div>
        <div class="row">
          <p><?php echo (empty($data['address_note'])) ? 'Няма' : $data['address_note']; ?></p>
        </div>
      </div>
      <div class="col-md-2 col-sm-12">
        <div class="row">
          <p>Област</p>
        </div>
        <div class="row">
          <p><?php echo $data['city']; ?></p>
        </div>
      </div>
      <div class="col-md-1 col-sm-12">
        <div class="row">
          <p>Брой места</p>
        </div>
        <div class="row">
          <p><?php echo $data['seats']; ?></p>
        </div>
      </div>
    </div>
    <?php if (!empty($data['events_you_may_like'])): ?>
      <div class="row">
        <div class="col-md-12 mt-5">
          <h3 class="text-center">Събития които може да харесате</h3><hr><hr>
        </div>

      </div>
      <div class="row">



        <?php foreach ($data['events_you_may_like'] as $ev): ?>
          <div class="col-md-4 col-sm-12">
            <div class="card">
              <img class="card-img-top" src="<?php echo URLROOT . '/' .$ev->images_url;?>" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title"><?php echo $ev->name;?></h5>
                <p class="card-text"><?php echo $ev->short_desc;?></p>
                <a href="<?php echo URLROOT; ?>/events/display?id=<?php echo $ev->id; ?>" class="btn btn-primary">Виж събитието</a>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
<script type="text/javascript">
  $.ready(function(){
    var interested = $('#interested');

  });

</script>
