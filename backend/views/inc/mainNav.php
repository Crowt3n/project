<nav class="navbar navbar-expand-md navbar-dark bg-dark">
<div class="container">
      <a class="navbar-brand" href="<?php echo URLROOT; ?>"><?php echo SITENAME; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>">Начало</a>
          </li>

          <?php if(isset($_SESSION['user_id'])) : ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/users/account">Моят профил</a>
            </li>
          <?php endif; ?>
          <?php if (isset($_SESSION['user_id'])): ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/events/categories">Събития</a>
            </li>
            <?php else: ?>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/events/index">Събития</a>
              </li>
          <?php endif; ?>
          <?php if(isset($_SESSION['admin_id'])) : ?>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/admins/index">Панел</a>
            </li>
          <?php endif; ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">За нас</a>
          </li>

          <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="https://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li> -->
        </ul>
        <ul class="navbar-nav ml-auto">
        <?php if (isset($_SESSION['admin_id'])): ?>
          <li class="nav-item">
            <a class="nav-link" href="#">Влезли сте като администратор</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/admins/logout">Изход</a>
          </li>
        <?php else: ?>
          <?php if(isset($_SESSION['user_id'])) : ?>
            <li class="nav-item">
              <div id="navbar-cart">
                <ul class="nav navbar-nav">
                  <li>
                    <a style="width:100%" href="#" id="cart-popover" class="btn btn-light" data-placement="bottom" title="Вашата количка">Kоличка
                      <span class="badge"></span>
                      <span class="total_price">BGN 0.00</span>

                    </a>
                  </li>
                </ul>

              </div>
              <div id="popover_content_wrapper" class="popover" style="display:none">
                <span id="cart_details"></span>
                <div align="right">
                  <a href="<?php URLROOT; ?>/tickets/checkoutStepOne" class="btn btn-primary" id="checkout_cart">Плащане</a>
                  <a href="#" class="btn btn-default" id="clear_cart">Изчисти количката</a>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Изход</a>
            </li>
          <?php else : ?>
          <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/users/login">Вход</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/users/register">Регистрация</a>
            </li>
          <?php endif; ?>
        <?php endif; ?>

        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="text" placeholder="Търси" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Търси</button>
        </form>
      </div>
      </div>
    </nav>
