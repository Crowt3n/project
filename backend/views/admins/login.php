<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">

    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
              <?php flash('register_success'); ?>
                <h2 class="text-center">Вход за администратори</h2>

            <form action="<?php echo URLROOT;?>/admins/login" method="post">
                <div class="form-group">
                    <label for="email">Мейл: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['email'];?>">
                    <span class="invalid-feedback"><?php echo $data['email_err'];?></span>
                </div>

                <div class="form-group">
                    <label for="password">Парола: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['password'];?>">
                    <span class="invalid-feedback"><?php echo $data['password_err'];?></span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Вход" class="btn btn-success btn-block">
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
