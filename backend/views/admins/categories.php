<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">

  <div class="row">

    <div class="col">
      <div class="row">
        <p class="display-4 mt-5">Редактиране на категория</p>
      </div>
      <div class="row">
        <select id="categ" class="form-control" name="cat_choice">
          <?php foreach ($data['categories'] as $category): ?>
            <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="row">
        <form class="" action="<?php URLROOT; ?>/admins/editCategories" method="post">
          <div class="form-group">
            <input class="form-control" type="hidden" name="cat_id" value="">
          </div>
          <div class="form-group">
            <label for="cat_name">Име на категория</label>
            <input class="form-control" type="text" name="cat_name" value="">
          </div>
          <div class="form-group">
            <label for="cat_desc">Описание на категория</label>
            <input  class="form-control" type="text" name="cat_desc" value="">
          </div>
          <div class="form-group">
            <input class="btn btn-success" type="submit" name="submit" value="Обнови">
          </div>
        </form>
      </div>
    </div>
    <div class="col">
      <div class="row">
        <p class="display-4 mt-5">Добавяне на категория</p>
      </div>
      <form class="" action="<?php URLROOT; ?>/admins/addCategory" method="post">
        <div class="form-group">
          <label for="name">Име на категория</label>
          <input class="form-control" type="text" name="name">
        </div>
        <div class="form-group">
          <label for="description">Описание на категория</label>
          <input class="form-control" type="text" name="description">
        </div>
        <div class="form-group">
          <input class="btn btn-primary" type="submit" name="submit" value="Добави">
        </div>
      </form>
    </div>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
<script type="text/javascript">
var id,name,desc;
  $(document).ready(function(){

    $('#categ').change(function(){
                  var catid = $("#categ").val();
                  $.ajax({
                    type:'POST',
                    url: 'http://localhost/admins/getCategory',
                    data:{category_id: catid},
                    dataType: 'json',
                    success: function(data, string, jqXHR){
                    data = $.parseJSON(JSON.stringify(data));

                    id = data.id;
                    name=data.name;
                    desc=data.description;
                    $("input[name*='cat_id']").val(id);
                    $("input[name*='cat_name']").val(name);
                    $("input[name*='cat_desc']").val(desc);
                    }
                  });
      });
    });


</script>
