<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row mt-5 mb-5">
    <div class="col-md-8">
      <div class="row">
        <form class="form-inline" action="<?php echo URLROOT; ?>/events/categories" method="post">
          <div class="form-group">
            <label for="from">От:</label>
            <input class="form-control" type="date" name="from" value="">
          </div>
          <div class="form-group ml-3">
            <label for="to">До:</label>
            <input class="form-control" type="date" name="to" value="">
          </div>
          <div class="form-group ml-3">
            <input class="btn btn-primary" type="submit" name="submit" value="Търси">
          </div>
        </form>
      </div>

    </div>
    <div class="col-md-4">
      <form class="form-inline" action="<?php echo URLROOT; ?>/events/categories" method="post">
        <select class="form-control" name="city_name">
          <?php foreach ($data['cities'] as $city): ?>
            <option value="<?php echo $city->name; ?>"><?php echo $city->name; ?></option>
          <?php endforeach; ?>
        </select>
        <input class="btn btn-primary ml-3" type="submit" name="submit" value="Търси">
      </form>
    </div>
  </div>
  <div class="row mb-5">
      <ul class="nav mx-auto">
        <?php foreach ($data['categories'] as $category): ?>
          <div class="col">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo URLROOT; ?>/events/categories?cat_id=<?php echo $category->id; ?>"><?php echo $category->name; ?></a>
            </li>
          </div>

        <?php endforeach; ?>
      </ul>
  </div>
  <div class="row">
    <?php if (!empty($data['no_events'])): ?>
      <div class="col">
        <h5 class="text-center"><?php echo $data['no_events']; ?></h5>
      </div>

      <?php else: ?>
        <?php foreach ($data['events'] as $event): ?>
          <div class="col-md-4 col-xs-12">
            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="<?php echo URLROOT . '/' .$event->images_url;?>" alt="Event image">
              <div class="card-body text-center">
                <h5 class="card-title"><?php echo $event->name; ?></h5>
                <p class="card-text"><?php echo $event->short_desc; ?></p>
                <a href="<?php echo URLROOT;?>/events/display?id=<?php echo $event->id;?>" class="btn btn-primary">Виж събитието</a>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
    <?php endif; ?>

  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
