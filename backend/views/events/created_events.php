<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
        <div class="col-md-9 col-sm-12 ml-auto mt-5">
          <?php if(isset($data['no_events'])):?>
            <div class="row mt-5">
              <div class="col-md-8">
                <h4 class="text-center">Не сте създали събития.</h4>
              </div>
              <div class="col-md-4">
                <a href="<?php echo URLROOT; ?>/events/create" class="btn btn-primary">Създай събитие</a>
              </div>
            </div>
          <?php else: ?>

          <h2 class="text-center">Вашите създадени събития</h2>
          <div class="row mt-5">
            <div class="col-md-2">
              <p>Име на събитието</p>
            </div>
            <div class="col-md-3">
              <p>Описание</p>
            </div>
            <div class="col-md-3">
              <p>Адрес</p>
            </div>
            <div class="col-md-3">
              <a class="btn btn-success" href="<?php echo URLROOT;?>/events/create">Създай събитие</a>
            </div>
          </div>
          <hr><hr>
            <?php foreach ($data as $event): ?>
              <div class="row">
                <div class="col-md-2">
                  <?php echo $event->name;?>
                </div>
                <div class="col-md-3">
                  <?php echo $event->short_desc;?>
                </div>
                <div class="col-md-3">
                  <?php echo $event->address;?>
                </div>
                <div class="col-md-2">
                  <a class="btn btn-success" href="<?php echo URLROOT;?>/events/edit?id=<?php echo $event->id;?>">Редактирай</a>
                </div>
                <div class="col-md-2">
                  <a class="btn btn-primary" href="<?php echo URLROOT;?>/events/display?id=<?php echo $event->id;?>">Виж събитието</a>
                </div>
              </div>
              <hr>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
