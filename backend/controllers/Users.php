<?php
class Users extends Controller{
    public function __construct(){
        $this->userModel=$this->model('User');
    }

    public function register(){
        //check if the user is logged in

        if($this->isLoggedIn()){
            redirect('events');
        }

        // check for POST request
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            //sanitize POST ???????????
            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'first_name' => trim($_POST['first_name']),
                'last_name' => trim($_POST['last_name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'first_name_err' => '',
                'last_name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',

            ];
            //validate email
            if(empty($data['email'])){
                $data['email_err'] = 'Моля въведете мейл';
                //validate first and last name

                if(empty($data['first_name'])){
                  $data['first_name_err'] = 'Моля въведете име';
                } else if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['first_name'])){
                  $data['first_name_err'] = 'Невалидно име';
                }
                if(empty($data['last_name'])){
                    $data['last_name_err'] = 'Моля въведете фамилия';
                } else if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['last_name'])){
                  $data['last_name_err'] = 'Невалидна фамилия';
                }
            } else{
                if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                  $data['email_err'] = 'Невалиден мейл адрес';
                }
                // Check Email
                if($this->userModel->findUserByEmail($data['email'])){
                  $data['email_err'] = 'Този мейл вече е използван';
                }
            }

            // Validate password
            if(empty($data['password'])){
                $password_err = 'Please enter a password.';
            } elseif(strlen($data['password']) < 6){
                $data['password_err'] = 'Паролата трябва да бъде с поне 6 символа';
            }

            // Validate confirm password
            if(empty($data['confirm_password'])){
                $data['confirm_password_err'] = 'Моля потвърдете паролата';
            } else{
                if($data['password'] != $data['confirm_password']){
                    $data['confirm_password_err'] = 'Паролите не съвпадат';
                }
            }

            // Make sure errors are empty
            if(empty($data['first_name_err']) && empty($data['last_name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
                // SUCCESS - Proceed to insert

                // Hash Password
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                //Execute
                if($this->userModel->register($data)){
                // Redirect to login
                flash('register_success', 'Регистрацията е успешна. Можете да влезете в системата');
                redirect('users/login');
                } else {
                die('Something went wrong');
                }

            } else {
                // Load View
                $this->view('users/register', $data);
            }


        } else {
            $form_data = [
                'first_name' => '',
                'last_name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'first_name_err' => '',
                'last_name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];
            $this->view('users/register', $form_data);
        }
    }

    public function login(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            ];

            if(empty($data['email'])){
                $data['email_err'] = 'Моля добавете мейл';
            }

            if(empty($data['password'])){
                $password_err = 'Моля добавете парола';
            }

            //check for usr email
            if($this->userModel->findUserByEmail($data['email'])){
                $loggedInUser = $this->userModel->login($data['email'], $data['password']);
                if($loggedInUser){
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['password_err'] = 'Грешна парола';
                    $this->view('users/login', $data);
                }
            } else {
                $data['email_err'] = 'Не съществува потребител с този мейл';
            }

            if(empty($data['email_err']) && empty($data['password_err'])){

            } else {

                $this->view('users/login', $data);
            }
        } else {
           $data = [
               'email'=>'',
               'password'=>'',
               'email_err'=>'',
               'password_err'=>'',
           ];

           $this->view('users/login', $data);
        }
    }

    public function createUserSession($user){
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_fn']);
        unset($_SESSION['admin_ln']);
        unset($_SESSION['admin_email']);
        unset($_SESSION['type']);
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_fn'] = $user->first_name;
        $_SESSION['user_ln'] = $user->last_name;
        $_SESSION['user_email'] = $user->email;

        redirect('pages/index');

    }

    public function logout(){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_fn']);
        unset($_SESSION['user_ln']);
        unset($_SESSION['user_email']);
        session_destroy();
        redirect('users/login');
    }

    public function isLoggedIn(){
        if(isset($_SESSION['user_id'])){
            return true;
        } else {
            return false;
        }
    }

    public function edit(){
      if(!isset($_SESSION['user_id'])){
        redirect('users/login');
      }

      if($_SERVER['REQUEST_METHOD'] == 'POST'){

        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $data = [
          'first_name' => trim($_POST['first_name']),
          'last_name' => trim($_POST['last_name']),
          'password' => trim($_POST['password']),
          'confirm_password' => trim($_POST['confirm_password']),

          'first_name_err' => '',
          'last_name_err' => '',
          'password_err' => '',
          'confirm_password_err' => '',
        ];
        if(empty($data['first_name'])){
          $data['first_name_err'] = 'Моля добавете име';
        } else if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['first_name'])){
          $data['first_name_err'] = 'Невалидно име';
        }
        if(empty($data['last_name'])){
            $data['last_name_err'] = 'Моля добавете фамилия';
        } else if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['last_name'])){
          $data['last_name_err'] = 'Невалидна фамилия';
        }

        if(empty($data['password'])){
            $password_err = 'Моля добавете парола';
        } elseif(strlen($data['password']) < 6){
            $data['password_err'] = 'Паролата трябва да е най-малко 6 символа';
        }

        // Validate confirm password
        if(empty($data['confirm_password'])){
            $data['confirm_password_err'] = 'Моля потвърдете парола';
        } else{
            if($data['password'] != $data['confirm_password']){
                $data['confirm_password_err'] = 'Паролите не съвпадат';
            }
        }

        if(empty($data['first_name_err']) && empty($data['last_name_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
          $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
          if ($this->userModel->updateCurrentUser($data)) {

            flash('register_success', 'Профилът е обновен');
            redirect('users/account');

          } else {
            die('Грешка при обновяване');
          }
        } else {

          $this->view('users/account', $data);
        }

      } else {
        $data = [
          'first_name'=>'',
          'last_name'=>'',
          'password'=>'',
          'confirm_password'=>'',
        ];
        $this->view('users/account', $data);
      }
    }

    public function account(){

      if ($obj = $this->userModel->loadCurrentUser()) {
        $data = [
          'first_name' => $obj->first_name,
          'last_name' => $obj->last_name,
        ];

        $this->view('users/account', $data);
      } else {
        redirect('users/login');
      }
    }

    public function notSentTickets(){
      if(!isset($_SESSION['user_id'])){
        redirect('users/login');
      }
      if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $id = $_POST['id'];
        $status = $_POST['status'];
        try {
          $this->userModel->orderSentUpdate($id, $status);
        } catch (\Exception $e) {}
      }
      try {
        $event_ids = $this->userModel->getEventsByUserId($_SESSION['user_id']);

        if ($event_ids) {
          foreach($event_ids as $event){
            $ids[] = $event->id;
          }
        }
        $orders = $this->userModel->notSentTickets($ids);
      } catch (\Exception $e) {}
        if ($orders) {
          $data['orders'] = $orders;
        } else {
          $data['no_orders'] = 'Няма чакащи поръчки.';
        }

        $this->view('users/waiting', $data);
    }

    public function completedOrders(){
      if(!isset($_SESSION['user_id'])){
        redirect('users/login');
      }

      try {
        $event_ids = $this->userModel->getEventsByUserId($_SESSION['user_id']);

        if ($event_ids) {
          foreach($event_ids as $event){
            $ids[] = $event->id;
          }
        }
        $orders = $this->userModel->completedOrders($ids);
      } catch (\Exception $e) {}
        if ($orders) {
          $data['orders'] = $orders;
        } else {
          $data['no_orders'] = 'Няма приключени поръчки.';
        }

        $this->view('users/completed', $data);
    }
}
