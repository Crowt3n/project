<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row mt-5">
    <div class="col-md-12">
      <h2 class="text-center">Плащане</h2>
    </div>
  </div>
  <form class="" action="<?php echo URLROOT; ?>/tickets/checkoutStepTwo" method="post">
  <div class="row mt-5">

    <div class="col-md-6 col-xs-12">
      <div class="row mb-5">
          <div class="col-md-12">
            <h3 class="text-center">Количка</h3>
          </div>
      </div>
      <div class="row">
        <div class="col-md-5">
          <p>Билет</p>
        </div>
        <div class="col-md-3">
          <p>Количество</p>
        </div>
        <div class="col-md-4">
          <p class="text-right">Цена</p>
        </div>
      </div>
      <hr>
      <?php foreach($data['tickets'] as $ticket) :?>
        <div class="row">
          <div class="col-md-5">
            <p><?php echo $ticket['product_name']; ?></p>
            <input type="hidden" name="name<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_name']; ?>">
            <input type="hidden" name="id<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_id']; ?>">
          </div>
          <div class="col-md-3">
            <p><?php echo $ticket['product_quantity']; ?></p>
            <input type="hidden" name="quantity<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_quantity']; ?>">
          </div>
          <div class="col-md-4">
            <p class="text-right">BGN <?php echo $ticket['product_price']; ?></p>
            <input type="hidden" name="price<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_price']; ?>">
          </div>
        </div>
        <hr>
      <?php endforeach; ?>
      <div class="row">
        <div class="col-md-3">
          <p>Общо:</p>
        </div>
        <div class="col-md-9">
          <p class="text-right">BGN <?php echo $data['total']; ?></p>
          <input type="hidden" name="total" value="<?php echo $data['total']; ?>">
        </div>
      </div>
    </div>

  <div class="col-md-6 col-xs-12">

      <div class="form-group">
        <label for="user_name">Име: </label>
        <input type="text" name="user_name" class="form-control form-control-lg <?php echo (!empty($data['user_name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['user_name'];?>">
        <span class="invalid-feedback"><?php echo $data['user_name_err'];?></span>
      </div>
      <div class="form-group">
        <label for="country">Държава: </label>
        <input type="text" name="country" class="form-control form-control-lg <?php echo (!empty($data['country_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['country'];?>">
        <span class="invalid-feedback"><?php echo $data['country_err'];?></span>
      </div>
      <div class="form-group">
        <label for="city">Град: </label>
        <input type="text" name="city" class="form-control form-control-lg <?php echo (!empty($data['city_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['city'];?>">
        <span class="invalid-feedback"><?php echo $data['city_err'];?></span>
      </div>
      <div class="form-group">
        <label for="address">Адрес: </label>
        <input type="text" name="address" class="form-control form-control-lg <?php echo (!empty($data['address_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['address'];?>">
        <span class="invalid-feedback"><?php echo $data['address_err'];?></span>
      </div>
      <div class="form-group">
        <input class="btn btn-primary" type="submit" name="submit" value="Продължи">
      </div>

  </div>
  </div>
    </form>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
