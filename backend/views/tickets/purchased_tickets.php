<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
        <div class="col-md-9 col-sm-12 ml-auto mt-5">
          <?php if (!empty($data['tickets'])): ?>

          <div class="row">
            <div class="col-md-12 mx-auto m-5">
              <h2 class="text-center">Вашите закупени билети</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <p class="text-center">Име на билет</p>
            </div>
            <div class="col-md-1">
              <p class="text-center">Количество</p>
            </div>
            <div class="col-md-3">
              <p class="text-center">Състояние</p>
            </div>
            <div class="col-md-4">
              <p class="text-center">Информация за доставка</p>
            </div>
            <div class="col-md-2">

            </div>
          </div>
          <hr><hr>
          <?php foreach($data['tickets'] as $ticket) :?>
          <div class="row">
            <div class="col-md-2">
              <p class="text-center"><?php echo $ticket->ticket_name; ?></p>
            </div>
            <div class="col-md-1">
              <p class="text-center"><?php echo $ticket->quantity; ?></p>
            </div>
            <div class="col-md-3">
              <p class="text-center"><?php echo $ticket->status ?></p>
            </div>
            <div class="col-md-4">
              <p class="text-center"><?php echo $ticket->country . ', ' . $ticket->city . ', '. $ticket->address . ', ' . $ticket->user_name; ?></p>
            </div>
            <div class="col-md-2">
              <a class="btn btn-primary" href="<?php echo URLROOT;?>/events/display?id=<?php echo $ticket->event_id;?>">Виж събитието</a>
            </div>

          </div>
          <hr>
        <?php endforeach; ?>
      <?php else: ?>
        <div class="row">
          <div class="col-md-12">
            <h4 class="text-center">Все още не сте закупили билети.</h4>
          </div>
        </div>
      <?php endif; ?>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
