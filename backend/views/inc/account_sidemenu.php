<div class="col-md-3 col-sm-12 ml-auto">
    <div class="card card-body bg-light mt-5">
        <h2 class="text-center">Моят профил</h2>

        <nav class="nav flex-column">
          <a class="nav-link" href="<?php echo URLROOT; ?>/events/createdEvents">Моите създадени събития</a>
          <a class="nav-link" href="<?php echo URLROOT; ?>/tickets/createdTickets">Моите създадени билети</a>
          <a class="nav-link" href="<?php echo URLROOT; ?>/events/attendedEvents">Събития на които ще присъствам</a>
          <a class="nav-link" href="<?php echo URLROOT; ?>/tickets/purchasedTickets">Моите закупени билети</a>
          <a class="nav-link" href="<?php echo URLROOT; ?>/users/notSentTickets">Чакащи поръчки</a>
          <a class="nav-link" href="<?php echo URLROOT; ?>/users/completedOrders">Приключени поръчки</a>
        </nav>
    </div>
</div>
