<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div id="index" class="jumbotron jumbotron-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="display-4 text-center">Готови ли сте да създадете Вашето събитие?<br>Може би искате да посетите друго?</h1>
      </div>

    </div>
      <div class="row">
        <div class="col-md-6 text-center">
          <a href="<?php echo URLROOT; ?>/events/categories" class="btn btn-success">Виж събитията</a>
        </div>
        <div class="col-md-6 text-center">
          <a href="<?php echo URLROOT; ?>/events/create" class="btn btn-primary">Създай своето събитие сега!</a>
        </div>
      </div>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
