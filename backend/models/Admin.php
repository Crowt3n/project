<?php

class Admin {
  private $db;

  public function __construct(){
      $this->db = new Database;
  }

  public function findAdminByEmail($email){
      $this->db->query('SELECT * FROM admins WHERE email = :email;');
      $this->db->bind(':email', $email);

      $row = $this->db->single();

      if($this->db->rowCount() > 0){
          return true;
      } else {
          return false;
      }
  }

  public function notApprovedEvents(){
    $this->db->query('SELECT * FROM events WHERE is_approved = 0');
    $row = $this->db->single();
    if ($this->db->rowCount() > 0) {
      return $this->db->rowCount();
    } else {
      return 0;
    }
  }

  public function getEventsToApprove(){
    $this->db->query('SELECT * FROM events WHERE is_approved = 0');
    $events = $this->db->resultSet();
    if ($this->db->rowCount() > 0) {
      return $events;
    } else {
      return false;
    }
  }

  public function approveEvent($event_id){
    $this->db->query('UPDATE events SET is_approved = 1 WHERE id = :id;');
    $this->db->bind(':id', $event_id);

    if($this->db->execute()){
        return true;
    } else {
        die('Events update failed');
    }
  }

  public function categoriesCount(){
    $this->db->query('SELECT id FROM categories');
    $row = $this->db->single();
    if ($this->db->rowCount() > 0) {
      return $this->db->rowCount();
    } else {
      return 0;
    }
  }

  public function citiesCount(){
    $this->db->query('SELECT id FROM cities');
    $row = $this->db->single();
    if ($this->db->rowCount() > 0) {
      return $this->db->rowCount();
    } else {
      return 0;
    }
  }

  public function getCategory($id){
    $this->db->query('SELECT * FROM categories WHERE id=:id');
    $this->db->bind(':id', $id);
    $row = $this->db->single();

    if ($this->db->rowCount()>0) {
      return $row;
    } else {
      return false;
    }
  }

  public function getCity($id){
    $this->db->query('SELECT * FROM cities WHERE id=:id');
    $this->db->bind(':id', $id);
    $row = $this->db->single();

    if ($this->db->rowCount()>0) {
      return $row;
    } else {
      return false;
    }
  }

  public function getAllCategories(){
    $this->db->query('SELECT * FROM categories');
    $result = $this->db->resultSet();

    if ($this->db->rowCount()>0) {
      return $result;
    } else {
      return false;
    }
  }

  public function getAllCities(){
    $this->db->query('SELECT * FROM cities');
    $result = $this->db->resultSet();

    if ($this->db->rowCount()>0) {
      return $result;
    } else {
      return false;
    }
  }

  public function updateCategory($id, $name, $description){
    $this->db->query('UPDATE categories SET name = :name, description=:description WHERE id=:id;');
    $this->db->bind(':name', $name);
    $this->db->bind(':description', $description);
    $this->db->bind('id', $id);

    if($this->db->execute()){
      return true;
    } else {
      die('Category update failed');
    }
  }

  public function updateCity($id, $name){
    $this->db->query('UPDATE cities SET name = :name WHERE id=:id;');
    $this->db->bind(':name', $name);
    $this->db->bind('id', $id);

    if($this->db->execute()){
      return true;
    } else {
      die('City update failed');
    }
  }

  public function addCategory($name, $description){
    $this->db->query('INSERT INTO categories (name, description) VALUES (:name, :description);');
    $this->db->bind(':name', $name);
    $this->db->bind(':description', $description);
    if($this->db->execute()){
      return true;
    } else {
      die('Category insert failed');
    }
  }

  public function addCity($name){
    $this->db->query('INSERT INTO cities (name) VALUES (:name);');
    $this->db->bind(':name', $name);
    if($this->db->execute()){
      return true;
    } else {
      die('City insert failed');
    }
  }

  public function login($email, $password){
      $this->db->query('SELECT * FROM admins WHERE email = :email');
      $this->db->bind(':email', $email);

      $row = $this->db->single();

      $hashed_password = $row->password;
      if(password_verify($password, $hashed_password)){
          return $row;
      } else {
          return false;
      }
  }

}

 ?>
