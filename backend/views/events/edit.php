<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 mx-auto">
            <h2>Редактирате събитие</h2>
            <div class="card card-body bg-light mt-5">
                <form action="<?php echo URLROOT;?>/events/edit" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="name">Име на събитието: <sup>*</sup></label>
                            <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['name'];?>">
                            <span class="invalid-feedback"><?php echo $data['name_err'];?></span>
                        </div>
                        <div class="form-group col">
                            <label for="type">Тип на събитието: <sup>*</sup></label>
                            <select name="type" class="form-control form-control-lg">
                                <option value="free">Безплатно</option>
                                <option value="paid">Платено</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="short_description">Кратко описание: </label>
                        <input type="text" name="short_description" class="form-control form-control-lg <?php echo (!empty($data['short_description_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['short_description'];?>">
                        <span class="invalid-feedback"><?php echo $data['short_description_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="description">Описание: <sup>*</sup></label>
                        <textarea name="description" cols="30" rows="8" class="form-control form-control-lg <?php echo (!empty($data['description_err'])) ? 'is-invalid' : '';?>"><?php echo $data['description'];?></textarea>
                        <span class="invalid-feedback"><?php echo $data['description_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="date_start">Стартира на: </label>
                        <input type="datetime-local" name="date_start" class="form-control form-control-lg <?php echo (!empty($data['date_start_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['date_start'];?>">
                        <span class="invalid-feedback"><?php echo $data['date_start_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="date_end">Приключва на: </label>
                        <input type="datetime-local" name="date_end" class="form-control form-control-lg <?php echo (!empty($data['date_end_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['date_end'];?>">
                        <span class="invalid-feedback"><?php echo $data['date_end_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="address">Адрес: <sup>*</sup></label>
                        <input type="text" name="address" class="form-control form-control-lg <?php echo (!empty($data['address_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['address'];?>">
                        <span class="invalid-feedback"><?php echo $data['address_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="address_note">Бележка за адрес:</label>
                        <input type="text" name="address_note" class="form-control form-control-lg <?php echo (!empty($data['address_note_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['address_note'];?>">
                        <span class="invalid-feedback"><?php echo $data['address_note_err'];?></span>
                    </div>
                    <div class="form-group">
                        <label for="seats">Брой места:</label>
                        <input type="text" name="seats" class="form-control form-control-lg <?php echo (!empty($data['seats_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['seats'];?>">
                        <span class="invalid-feedback"><?php echo $data['seats_err'];?></span>
                    </div>
                    <div class="form-group">
                    <input type="submit" name="update" value="Update" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
