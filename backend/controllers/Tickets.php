<?php

class Tickets extends Controller {

  public function __construct(){
      $this->ticketModel=$this->model('Ticket');
  }

  public function index(){
    $this->view('tickets/index');
  }

  public function clearCart(){
    session_start();

    if(isset($_POST["action"]))
    {

     if($_POST["action"] == 'remove')
     {
      foreach($_SESSION["shopping_cart"] as $keys => $values)
      {
       if($values["product_id"] == $_POST["product_id"])
       {
        unset($_SESSION["shopping_cart"][$keys]);
       }
      }
     }
     if($_POST["action"] == 'empty')
     {
      unset($_SESSION["shopping_cart"]);
     }
    }

  }

  public function addToCart(){

    session_start();

      if(isset($_POST["action"]))
      {
       if($_POST["action"] == "add")
       {
        if(isset($_SESSION['shopping_cart']))
        {
         $is_available = 0;
         foreach($_SESSION['shopping_cart'] as $keys => $values)
         {
          if($_SESSION['shopping_cart'][$keys]['product_id'] == $_POST["product_id"])
          {
           $is_available++;
           $_SESSION['shopping_cart'][$keys]['product_quantity'] = $_SESSION["shopping_cart"][$keys]['product_quantity'] + $_POST["product_quantity"];
          }
         }
         if($is_available == 0)
         {
          $item_array = array(
           'product_id'               =>     $_POST["product_id"],
           'product_name'             =>     $_POST["product_name"],
           'product_price'            =>     $_POST["product_price"],
           'product_quantity'         =>     $_POST["product_quantity"]
          );
          $_SESSION["shopping_cart"][] = $item_array;
         }
        }
        else
        {
         $item_array = array(
          'product_id'               =>     $_POST["product_id"],
          'product_name'             =>     $_POST["product_name"],
          'product_price'            =>     $_POST["product_price"],
          'product_quantity'         =>     $_POST["product_quantity"]
         );
         $_SESSION['shopping_cart'][] = $item_array;
        }
       }
      }
  }

  public function getCart(){
    session_start();
    $total_price = 0;
    $total_tickets = 0;

    $output = '
    <div class="table-responsive" id="order_table">
      <table class="table table-bordered table-striped">
        <tr>
          <th width="30%">Име на билет</th>
          <th width="10%">Количество</th>
          <th width="20%">Цена</th>
          <th width="20%">Общо</th>
          <th width="10%">Действие</th>
        </tr>
    ';

    if (!empty($_SESSION['shopping_cart'])) {
      foreach ($_SESSION['shopping_cart'] as $item) {
        $output .= '
        <tr>
          <td>'. $item['product_name'] .'</td>
          <td>'. $item['product_quantity'] .'</td>
          <td>'. $item['product_price'] .'</td>
          <td>'. $item['product_price'] * $item['product_quantity'] .'</td>
          <td><button name="delete" class="btn btn-danger btn-xs delete" id="'. $item['product_id'] .'" >Премахни</button></td>
        </tr>
        ';
        $total_price = $total_price + ($item['product_quantity'] * $item['product_price']);
        $total_items = $total_items + 1;

        $output .= '
        <tr>
          <td colspan="3" align="right">Общо </td>
          <td align="right">BGN'.$total_price.'</td>
          <td></td>
        </tr>
        ';
      }
    } else {
      $output.='
      <tr>
        <td colspan="5" align="center">Вашата количка е празна.</td>
      </tr>
      ';
    }

    $output .= '</table></div>';

    $data = [
      'cart_details' => $output,
      'total_price' => 'BGN '.$total_price,
      'total_items' => $total_items,
    ];

    echo json_encode($data);
  }

  public function create(){
    if(!isset($_SESSION['user_id'])){
      redirect('users/login');
    }
    if(isset($_GET['id'])){
      if($event = $this->ticketModel->getEventById($_GET['id'])){
        $data = [
          'event_id' => $event->id,
          'seats_open'=> $event->seats_open,
        ];
      }
    }
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

      $data = [
        'name'=>trim($_POST['name']),
        'description'=>trim($_POST['description']),
        'price'=>trim($_POST['price']),
        'count'=>trim($_POST['count']),
        'event_id'=> $_POST['event_id'],
        'seats_open'=> $_POST['seats_open'],

        'name_err'=>'',
        'description_err'=>'',
        'price_err'=>'',
        'count_err'=>'',
      ];

      if (empty($data['name'])) {
        $data['name_err'] = 'Моля добавете име на билет';
      } else {
        if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,100}$/", $data['name'])){
          $data['name_err'] = 'Името на билета може да съдържа само букви и да е не по-дълго от 50 символа.';
        }
      }

      if (empty($data['description'])) {
        $data['description_err'] = 'Моля добавете описание на билета';
      } else {
        if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}0-9]{2,100}$/", $data['description'])){
          $data['description_err'] = 'Описанието на билета може да съдържа само букви и цифри и да е не по-дълго от 100 символа.';
        }
      }

      if(empty($data['price'])){
        $data['price_err'] = 'Моля добавете цена за билет';
      } else {
        if(!preg_match("/^([1-9][0-9]*|0)(\.[0-9]{2})?$/", $data['price'])){
          $data['price_err'] = 'Моля добавете валидна цена';
        }
      }

      if (empty($data['count'])) {
        $data['count_err'] = 'Моля добавете количество на билетите';
      } else{
        if($data['count'] > $data['seats_open']){
          $data['count_err'] = 'Вашето събитие няма достатъчно свободни места';
        }
      }
      if(empty($data['name_err'])
      && empty($data['description_err'])
      && empty($data['count_err'])
      && empty($data['price_err'])){
        if ($this->ticketModel->create($data)) {
          $seats_left = $data['seats_open'] - $data['count'];
          $this->ticketModel->reduceSeatsOpen($data['event_id'],$seats_left);
          flash('ticket_created', 'Вашият билет беше създаден');
          redirect('tickets/createdTickets');
        }else{
          die('Грешка при създаване на билет');
        }
      } else {
        $this->view('tickets/create', $data);
      }
    } else {
      if(isset($_GET['id'])){
        if($event = $this->ticketModel->getEventById($_GET['id'])){
          $data = [
            'event_name' => $event->name,
            'event_id' => $event->id,
            'seats_open'=>$event->seats_open,
          ];
          $this->view('tickets/create', $data);
        }
      }
    }
  }

  public function createdTickets(){
    if(!isset($_SESSION['user_id'])){
      redirect('users/login');
    }

    if($data = $this->ticketModel->createdTickets($_SESSION['user_id'])){
      $this->view('tickets/created_tickets', $data);
    } else {
      $data['no_tickets'] = 'Все още не сте създали билети за това събитие';
      $this->view('tickets/created_tickets', $data);
    }
  }

  public function list(){
    if(!isset($_SESSION['user_id'])){
      redirect('users/login');
    }

    if(isset($_GET['id'])){
      if ($data = $this->ticketModel->list($_GET['id'])) {
        $event = $this->ticketModel->getEventById($_GET['id']);
        $data['event_name'] = $event->name;
        $this->view('tickets/list', $data);
      }
    } else {
      $data['no_tickets'] = 'Все още няма билети за това събитие';
      $this->view('tickets/list', $data);
    }
  }

  public function edit(){
    if(!isset($_SESSION['user_id'])){
      redirect('users/login');
    }
    if(isset($_GET['id'])){
      if($ticket = $this->ticketModel->getTicketById($_GET['id'])){
        $data = [
          'id'=> $ticket->id,
          'event_id' => $ticket->event_id,
          'name' => $ticket->name,
          'description' => $ticket->description,
          'current_count' => $ticket->total,
          'count'=>$ticket->total,
          'tickets_left' => $ticket->remaining,
          'price' => $ticket->price,

        ];
        if($event = $this->ticketModel->getEventById($ticket->event_id)){
          $data['seats_open'] = $event->seats_open;
          $data['event_start'] = $event->date_start;
          $data['event_name'] = $event->name;
        }
      }
    } else {
      redirect('tickets/createdTickets');
    }
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

      $data = [
        'name'=>trim($_POST['name']),
        'description'=>trim($_POST['description']),
        'price'=>trim($_POST['price']),
        'count'=>trim($_POST['count']),
        'event_id'=> $_POST['event_id'],
        'seats_open'=> $_POST['seats_open'],
        'current_count' => $_POST['current_count'],
        'tickets_left' => $_POST['tickets_left'],
        'event_start' => $_POST['event_start'],

        'name_err'=>'',
        'description_err'=>'',
        'price_err'=>'',
        'count_err'=>'',
        'cant_edit'=>'',
      ];
      if (strtotime($data['event_start']) < strtotime('1 week ago') || $data['current_count'] != $data['tickets_left']) {
        $data['cant_edit'] = 'Не можете да редактирате този билет защото събитието започва след по-малко от седмица или някои от билетите вече са продадени.';
      }
      if (empty($data['name'])) {
        $data['name_err'] = 'Моля добавете име на билет';
      } else {
        if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,50}$/", $data['name'])){
          $data['name_err'] = 'Името може да съдържа само букви и да е не по-дълго от 50 символа.';
        }
      }

      if (empty($data['description'])) {
        $data['description_err'] = 'Моля добавете описание';
      } else {
        if(!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}0-9]{2,100}$/", $data['description'])){
          $data['description_err'] = 'Описанието може да съдържа букви и цифри и да е не по-дълго от 100 символа.';
        }
      }

      if(empty($data['price'])){
        $data['price_err'] = 'Моля добавете цена за билет';
      } else {
        if(!preg_match("/^([1-9][0-9]*|0)(\.[0-9]{2})?$/", $data['price'])){
          $data['price_err'] = 'Невалидна цена на билет';
        }
      }

      if (empty($data['count'])) {
        $data['count_err'] = 'Моля добавете количество';
      } else{
        if($data['count'] > ($data['seats_open'] + $data['current_count'])){
          $data['count_err'] = 'Вашето събитие няма достатъчно свободни места';
        }
      }
      if(empty($data['name_err'])
      && empty($data['description_err'])
      && empty($data['count_err'])
      && empty($data['price_err'])
      && empty($data['cant_edit'])
    ){
        if ($this->ticketModel->update($data)) {
          $seats_left = $data['seats_open'] + $data['current_count'] - $data['count'];
          $this->ticketModel->reduceSeatsOpen($data['event_id'],$seats_left);
          flash('ticket_updated', 'Билетът беше обновен');
         redirect('tickets/createdTickets');
        }else{
          die('Грешка при обновяване');
        }
      } else {
        $this->view('tickets/edit', $data);
      }
    } else {
      if(isset($_GET['id'])){
        if($ticket = $this->ticketModel->getTicketById($_GET['id'])){
          $data = [
            'id'=> $ticket->id,
            'event_id' => $ticket->event_id,
            'name' => $ticket->name,
            'description' => $ticket->description,
            'current_count' => $ticket->total,
            'count' => $ticket->total,
            'tickets_left' => $ticket->remaining,
            'price' => $ticket->price,

          ];
          if($event = $this->ticketModel->getEventById($ticket->event_id)){
            $data['seats_open'] = $event->seats_open;
            $data['event_start'] = $event->date_start;
            $data['event_name'] = $event->name;
          }
          $this->view('tickets/edit', $data);
        }
      }
    }
  }

  public function buy(){
    if (isset($_GET['id'])) {
      $data['tickets'] = $this->ticketModel->list($_GET['id']);
      $data['event'] = $this->ticketModel->getEventById($_GET['id']);

      foreach($data['tickets'] as $ticket){
        $output .= '<div class="row">
          <div class="col-md-2">
            <p>'. $ticket->name .'</p>
          </div>
          <div class="col-md-2">
            <p>'.$ticket->description .'</p>
          </div>
          <div class="col-md-2">
            <p>'.$ticket->remaining.'</p>
          </div>
          <div class="col-md-2">
            <p>BGN '.$ticket->price.'</p>
          </div>
          <div class="col-md-2">
            <div class="form-group">
                <input type="number" name="quantity" id="quantity'. $ticket->id.'" class="form-control" value="1">
            </div>
          </div>
          <div class="col-md-2">
            <input type="hidden" name="name" id="name'.$ticket->id.'" value="'.$ticket->name.'">
            <input type="hidden" name="price" id="price'.$ticket->id.'" value="'.$ticket->price.'">
            <input type="button" name="add_to_cart" id="'. $ticket->id.'" class="btn btn-success form-control add_to_cart" value="Добави в количката">
          </div>
        </div>
        <hr>
        ';
      }

      $data['output'] = $output;

      $this->view('tickets/buy', $data);
    } else {
      redirect('tickets/index');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // to do
    }

  }

  public function checkoutStepOne(){
    if (!isset($_SESSION['user_id'])) {
      redirect('users/login');
    }

    $data['cart_items'] = $_SESSION['shopping_cart'];
    $ids = array();
    $data['total']='';
    foreach($data['cart_items'] as $ticket){
      $ids[] = $ticket['product_id'];
    }

    if($tickets = $this->ticketModel->getTicketsWhereIds($ids)){

      foreach($data['cart_items'] as $item){

        foreach($tickets as $ticket){
          if ($item['product_id'] == $ticket->id) {
            $item['event_id'] = $ticket->event_id;

            if(!($item['product_quantity'] < $ticket->remaining)){
              $item['quantity_err'] = 'Билетите са продадени';
            }
            $data['total'] = $data['total'] + ($item['product_quantity'] * $item['product_price']);
            $data['tickets'][] = $item;
          }
        }
      }
    }


    $this->view('tickets/checkout', $data);
  }

  public function checkoutStepTwo(){

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data = [
            'user_name' => trim($_POST['user_name']),
            'country' => trim($_POST['country']),
            'city' => trim($_POST['city']),
            'address' => trim($_POST['address']),

            'user_name_err' => '',
            'country_err' => '',
            'city_err' => '',
            'address_err' => '',
        ];

        if (empty($data['user_name'])) {
            $data['user_name_err'] = 'Моля въведете име за доставка';
        } else {
          if (!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['user_name'])) {
            $data['user_name_err'] = 'Invalid name.';
          }
        }

        if (empty($data['country'])) {
            $data['country_err'] = 'Моля въведете държава';
        } else {
          if (!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['country'])) {
            $data['country_err'] = 'Невалидна държава';
          }
        }

        if (empty($data['city'])) {
            $data['city_err'] = 'Моля добавете град';
        } else {
          if (!preg_match("/[a-zA-Zа-яА-Я\p{Cyrillic}]{2,30}$/", $data['city'])) {
            $data['city_err'] = 'Невалиден град';
          }
        }

        if (empty($data['address'])) {
          $data['address_err'] = 'Моля добавете адрес за доставка';
        }

        if (empty($data['user_name_err'])
          && empty($data['country_err'])
          && empty($data['city_err'])
          && empty($data['address_err'])) {

          $this->checkoutConfirm($data);
        } else {
          $this->checkoutStepOne();
        }
    }
  }

  public function checkoutConfirm($data){
    $data['total'] = '';
    foreach($_SESSION['shopping_cart'] as $item){
      $data['items'][] = $item;
      $data['total'] = $data['total'] + ($item['product_quantity'] * $item['product_price']);
    }

    $this->view('tickets/checkout_confirm', $data);
  }

  public function complete(){
    if($_SERVER['REQUEST_METHOD'] = 'POST'){
      $session_cart = $_SESSION['shopping_cart'];
      unset($_SESSION['shopping_cart']);
      $status = 'Готов за изпращане';
      $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
      $username = $_POST['user_name'];
      $country = $_POST['country'];
      $city = $_POST['city'];
      $address = $_POST['address'];

      foreach($session_cart as $ticket){
        $ids[] = $ticket['product_id'];
      }
      //update tickets quantity
      if ($tickets = $this->ticketModel->getTicketsWhereIds($ids)) {
        foreach($session_cart as $item){

          foreach($tickets as $ticket){
            if ($item['product_id'] == $ticket->id) {
              $ticket->remaining = $ticket->remaining - $item['product_quantity'];
              $this->ticketModel->updateTicket($ticket);
              //insert in purchased tickets
              $this->ticketModel->purchasedTickets($_SESSION['user_id'], $item['product_id'], $item['product_quantity'], $status, $address, $city, $country, $username, $ticket->event_id, $ticket->name);
              // insert in attendance
              $this->ticketModel->addAtendance($_SESSION['user_id'], $ticket->event_id, $ticket->id);
            }
          }
        }
      }

      redirect('tickets/purchasedTickets');

    }
  }

  public function purchasedTickets(){
    if (!isset($_SESSION['user_id'])) {
      redirect('users/login');
    }

    $purchased_tickets = $this->ticketModel->getPurchasedTickets($_SESSION['user_id']);

    $data['tickets'] = $purchased_tickets;

    $this->view('tickets/purchased_tickets', $data);
  }
}
 ?>
