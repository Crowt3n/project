<?php
class Admins extends Controller{
    public function __construct(){
        $this->adminModel=$this->model('Admin');
    }

    public function index(){
      $data = array();
      $data['need_approval'] = $this->adminModel->notApprovedEvents();
      $data['categories'] = $this->adminModel->categoriesCount();
      $data['cities'] = $this->adminModel->citiesCount();
      $this->view('admins/index', $data);
    }

    public function login(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            ];

            if(empty($data['email'])){
                $data['email_err'] = 'Моля добавете мейл';
            }

            if(empty($data['password'])){
                $password_err = 'Моля добавете парола';
            }

            //check for admin email
            if($this->adminModel->findAdminByEmail($data['email'])){
                $loggedInAdmin = $this->adminModel->login($data['email'], $data['password']);
                if($loggedInAdmin){
                    $this->createAdminSession($loggedInAdmin);
                } else {
                    $data['password_err'] = 'Грешна парола';
                    $this->view('admins/login', $data);
                }
            } else {
                $data['email_err'] = 'Не съществува потребител с този мейл';
            }

            if(empty($data['email_err']) && empty($data['password_err'])){

            } else {

                $this->view('admins/login', $data);
            }
        } else {
           $data = [
               'email'=>'',
               'password'=>'',
               'email_err'=>'',
               'password_err'=>'',
           ];

           $this->view('admins/login', $data);
        }
    }

    public function logout(){
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_fn']);
        unset($_SESSION['admin_ln']);
        unset($_SESSION['admin_email']);
        unset($_SESSION['type']);
        session_destroy();
        redirect('pages/index');
    }

    public function createAdminSession($admin){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_fn']);
        unset($_SESSION['user_ln']);
        unset($_SESSION['user_email']);
        $_SESSION['admin_id'] = $admin->id;
        $_SESSION['admin_fn'] = $admin->first_name;
        $_SESSION['admin_ln'] = $admin->last_name;
        $_SESSION['admin_email'] = $admin->email;
        $_SESSION['type'] = 'admin';


        redirect('admins/index');

    }

    public function needsApproval(){
      if (!isset($_SESSION['admin_id'])) {
        redirect('admins/login');
      }
      if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(is_numeric($_POST['event_id'])){
          $event_id = $_POST['event_id'];
          try {
            $this->adminModel->approveEvent($event_id);
          } catch (\Exception $e) {}
            $data['events'] = $this->adminModel->getEventsToApprove();
            if($data['events']){
              $this->view('admins/approval', $data);
            }

        }
      } else {
        $data['events'] = $this->adminModel->getEventsToApprove();
        if($data['events']){
          $this->view('admins/approval', $data);
        }
      }
    }

    public function addCategory(){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data = [
          'name'=>$_POST['name'],
          'description'=>$_POST['description'],

          'name_err' =>'',
          'desc_err' =>'',
        ];

        if (empty($data['name'])) {
          // code...
        }

        if (empty($data['description'])) {
          // code...
        }

        if (empty($data['name_err']) && empty($data['desc_err'])) {
          try {
            $this->adminModel->addCategory($data['name'], $data['description']);
          } catch (\Exception $e) {

          }
          redirect('admins/editCategories');
        }
      }
    }

    public function addCity(){
      if($_SERVER['REQUEST_METHOD'] == "POST"){
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data = [
          'name'=>$_POST['name'],

          'name_err' =>'',
        ];

        if (empty($data['name'])) {
          // code...
        }


        if (empty($data['name_err'])) {
          try {
            $this->adminModel->addCity($data['name']);
          } catch (\Exception $e) {

          }
          redirect('admins/editCities');
        }
      }
    }

    public function getCategory(){

      if(isset($_POST['category_id'])){
          $id = $_POST['category_id'];

          $category = $this->adminModel->getCategory($id);
          $data=[
            'id'=>$category->id,
            'name'=>$category->name,
            'description'=>$category->description,
          ];
          $data = json_encode($data, JSON_UNESCAPED_UNICODE);
          echo $data;
        ;
      }

    }

    public function getCity(){

      if(isset($_POST['city_id'])){
          $id = $_POST['city_id'];

          $city = $this->adminModel->getCity($id);
          $data=[
            'id'=>$city->id,
            'name'=>$city->name,
          ];
          $data = json_encode($data, JSON_UNESCAPED_UNICODE);
          echo $data;
        ;
      }

    }

    public function editCategories(){
      if (!isset($_SESSION['admin_id'])) {
        redirect('admins/login');
      }
      if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data['cat_id'] = $_POST['cat_id'];
        $data['cat_name'] = $_POST['cat_name'];
        $data['cat_desc'] = $_POST['cat_desc'];
        try {
          $this->adminModel->updateCategory($data['cat_id'], $data['cat_name'], $data['cat_desc']);
        } catch (\Exception $e) {}
        $data['categories'] = $this->adminModel->getAllCategories();
        $data['cat_id'] = $data['cat_name'] =  $data['cat_desc'] = '';
        redirect('admins/editCategories');
      } else {
        $data['categories'] = $this->adminModel->getAllCategories();
        $this->view('admins/categories', $data);
      }
    }

    public function editCities(){
      if (!isset($_SESSION['admin_id'])) {
        redirect('admins/login');
      }
      if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $data['city_id'] = $_POST['city_id'];
        $data['city_name'] = $_POST['city_name'];

        try {
          $this->adminModel->updateCity($data['city_id'], $data['city_name']);
        } catch (\Exception $e) {}
        $data['cities'] = $this->adminModel->getAllCities();
        $data['city_id'] = $data['city_name'] =  $data['city_desc'] = '';
        $this->view('admins/cities', $data);
      } else {
        $data['cities'] = $this->adminModel->getAllCities();
        $this->view('admins/cities', $data);
      }
    }





}

 ?>
