<?php
    define('DB_HOST', 'localhost');
    define('DB_USER','root');
    define('DB_PASS','password');
    define('DB_NAME','events');
    //Backend root
    define('BACKENDROOT', dirname(dirname(__FILE__)));
    //URL root
    define('URLROOT', 'http://localhost');
    //Site name
    define('SITENAME', 'Events');
