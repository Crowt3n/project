<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row">
    <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
    <div class="col-md-9 col-sm-12 ml-auto mt-5">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <h2 class="text-center">Редактирате билет</h2>
        </div>
      </div>
      <div class="row  mt-2 mb-2">
        <div class="col-md-6 col-sm-12">
          <p>Редактирате билет за следното събитие:</p>
        </div>
        <div class="col-md-6 col-sm-12">
          <p><?php echo $data['event_name']; ?></p>
        </div>
      </div>
      <?php if(!empty($data['cant_edit'])) :?>
      <div class="row">
        <div class="alert alert-danger">
          <?php echo $data['cant_edit']; ?>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="<?php echo URLROOT;?>/tickets/edit?id=<?php echo $data['id']; ?>">
            <div class="form-group">
                <label for="name">Име на билет: </label>
                <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['name'];?>">
                <span class="invalid-feedback"><?php echo $data['name_err'];?></span>
            </div>
            <div class="form-group">
                <label for="description">Описание на билет: </label>
                <input type="text" name="description" class="form-control form-control-lg <?php echo (!empty($data['description_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['description'];?>">
                <span class="invalid-feedback"><?php echo $data['description_err'];?></span>
            </div>
            <div class="form-group">
                <label for="price">Цена на билет: </label>
                <input type="number" min="1" step="any" name="price" class="form-control form-control-lg <?php echo (!empty($data['price_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['price'];?>">
                <span class="invalid-feedback"><?php echo $data['price_err'];?></span>
            </div>
            <div class="form-group">
                <label for="count">Количество билети:</label>
                <input type="number" name="count" class="form-control form-control-lg <?php echo (!empty($data['count_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['count'];?>">
                <span class="invalid-feedback"><?php echo $data['count_err'];?></span>
            </div>
            <input type="hidden" name="event_id" value="<?php echo $data['event_id']; ?>">
            <input type="hidden" name="current_count" value="<?php echo $data['current_count']; ?>">
            <input type="hidden" name="event_start" value="<?php echo $data['event_start']; ?>">
            <input type="hidden" name="tickets_left" value="<?php echo $data['tickets_left']; ?>">
            <input type="hidden" name="seats_open" value="<?php echo $data['seats_open']; ?>">

            <div class="form-group">
              <input class="btn btn-primary" type="submit" name="submit" value="Update ticket">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
