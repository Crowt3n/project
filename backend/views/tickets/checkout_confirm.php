<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <form action="<?php echo URLROOT; ?>/tickets/complete" method="post">

  <div class="row mt-5">
      <div class="col m-2">
        <div class="row">
          <div class="col-md-12 mb-5">
            <h3 class="text-center">Количка</h3>
          </div>

        </div>
        <div class="row">
          <div class="col-md-5">
            <p>Билет</p>
          </div>
          <div class="col-md-3">
            <p>Количество</p>
          </div>
          <div class="col-md-4">
            <p class="text-right">Цена</p>
          </div>
        </div>
        <hr>
        <?php foreach($data['items'] as $ticket):?>
          <div class="row">
            <div class="col-md-5">
              <p><?php echo $ticket['product_name']; ?></p>
              <input type="hidden" name="name<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_name']; ?>">
              <input type="hidden" name="id<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_id']; ?>">
            </div>
            <div class="col-md-3">
              <p><?php echo $ticket['product_quantity']; ?></p>
              <input type="hidden" name="quantity<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_quantity']; ?>">
            </div>
            <div class="col-md-4">
              <p class="text-right">BGN <?php echo $ticket['product_price']; ?></p>
              <input type="hidden" name="price<?php echo $ticket['product_id']; ?>" value="<?php echo $ticket['product_price']; ?>">
            </div>
          </div>
          <hr>
        <?php endforeach; ?>
        <div class="row">
          <div class="col-md-12">
            <p>Общо всичко: BGN <?php echo $data['total']; ?></p>
          </div>
        </div>
      </div>
      <div class="col m-2">
        <div class="row">
          <div class="col-md-12 mb-5">
            <h3 class="text-center">Информация за доставка</h3>
          </div>
        </div>
        <div class="row">
          <p>Име: <?php echo $data['user_name']; ?></p>
          <input type="hidden" name="user_name" value="<?php echo $data['user_name']; ?>">
        </div>
        <div class="row">
          <p>Държава: <?php echo $data['country']; ?></p>
          <input type="hidden" name="country" value="<?php echo $data['country']; ?>">
        </div>
        <div class="row">
          <p>Град: <?php echo $data['city']; ?></p>
          <input type="hidden" name="city" value="<?php echo $data['city']; ?>">
        </div>
        <div class="row">
          <p>Адрес: <?php echo $data['address']; ?></p>
          <input type="hidden" name="address" value="<?php echo $data['address']; ?>">
        </div>
      </div>
  </div>
  <div class="row">
      <div class="col">
          <input class="btn btn-success text-center" type="submit" name="submit" value="Поръчай">
      </div>
  </div>
</form>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
