<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row">
    <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
    <div class="col-md-9 col-sm-12 ml-auto mt-5">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <h2 class="text-center">Списък с билети</h2>
        </div>
      </div>
      <div class="row  mt-2 mb-2">
        <div class="col-md-6 col-sm-12">
          <p>Виждате билети за следното събитие: </p>
        </div>
        <div class="col-md-6 col-sm-12">
          <p><?php echo $data['event_name']; ?></p>
        </div>
      </div>
      <?php if(!empty($data['no_tickets'])): ?>
        <div class="row">
          <div class="alert alert-danger">
            <?php echo $data['no_tickets'];?>
          </div>

        </div>
      <?php else: ?>

      <div class="row">
          <div class="col-md-2">
            <p>Име на билет</p>
          </div>
          <div class="col-md-2">
            <p>Описание</p>
          </div>
          <div class="col-md-2">
            <p>Брой билети</p>
          </div>
          <div class="col-md-2">
            <p>Оставащи билети</p>
          </div>
          <div class="col-md-4">
            <p>Цена</p>
          </div>
      </div>
      <?php foreach($data as $ticket): ?>
        <?php if (is_object($ticket)): ?>

        <div class="row">
            <div class="col-md-2">
              <p><?php echo $ticket->name; ?></p>
            </div>
            <div class="col-md-2">
              <p><?php echo $ticket->description; ?></p>
            </div>
            <div class="col-md-2">
              <p><?php echo $ticket->total; ?></p>
            </div>
            <div class="col-md-2">
              <p><?php echo $ticket->remaining; ?></p>
            </div>
            <div class="col-md-2">
              <p><?php echo $ticket->price; ?></p>
            </div>
            <div class="col-md-2">
              <a class="btn btn-primary" href="<?php echo URLROOT; ?>/tickets/edit?id=<?php echo $ticket->id; ?>">Редактирай</a>
            </div>
        </div>
        <hr>
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
      <?php endif; ?>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
