<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div id="event-index" class="jumbotron jumbotron-fluid">
  <div class="container">
    <div class="row">
      <div class="col">
          <div class="card" style="height: 450px;">
            <img class="card-img-top" src="<?php echo URLROOT . '/images/core/approve_events.png';?>" style="height:60%;" alt="Event image">
            <div class="card-body text-center">
              <h5 class="card-title">Събития</h5>
              <p class="card-text">Чакащи потвърждение: <?php echo $data['need_approval']; ?></p>
              <a href="<?php echo URLROOT;?>/admins/needsApproval" class="btn btn-primary">Преглед</a>
            </div>
          </div>
      </div>
      <div class="col">
        <div class="card text-center" style="height: 450px;">
          <img class="card-img-top mx-auto" src="<?php echo URLROOT . '/images/core/categories.png';?>" style="height:60%;width:80%;" alt="Event image">
          <div class="card-body text-center">
            <h5 class="card-title">Категории</h5>
            <p class="card-text">Общо категории: <?php echo $data['categories']; ?></p>
            <a href="<?php echo URLROOT;?>/admins/editCategories" class="btn btn-primary">Редактиране</a>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="card" style="height: 450px;">
          <img class="card-img-top mx-auto" src="<?php echo URLROOT . '/images/core/location.png';?>" style="height:60%;width:80%;"alt="Event image">
          <div class="card-body text-center">
            <h5 class="card-title">Градове</h5>
            <p class="card-text">Общо градове: <?php echo $data['cities']; ?></p>
            <a href="<?php echo URLROOT;?>/admins/editCities" class="btn btn-primary">Редактиране</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
