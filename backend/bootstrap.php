<?php
    //Load config
    require_once 'config/config.php';
    //Load helpers
    require_once 'helpers/url_redirect.php';
    require_once 'helpers/flash_messages.php';
    //Autoload core libraries
    spl_autoload_register(function($className){
        require_once 'libraries/' . $className . '.php';
    });
