<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
    <div class="row">
        <?php require BACKENDROOT . '/views/inc/account_sidemenu.php'; ?>
        <div class="col-md-9 col-sm-12 ml-auto mt-5">
          <h2 class="text-center">Информация за Вашият профил</h2>
          <form action="<?php echo URLROOT;?>/users/edit" method="post">
                <div class="form-group">
                    <label for="first_name">Име:</label>
                    <input type="text" name="first_name" class="form-control form-control-lg <?php echo (!empty($data['first_name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['first_name'];?>">
                    <span class="invalid-feedback"><?php echo $data['first_name_err'];?></span>
                </div>
                <div class="form-group">
                    <label for="last_name">Фамилия:</label>
                    <input type="text" name="last_name" class="form-control form-control-lg <?php echo (!empty($data['last_name_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['last_name'];?>">
                    <span class="invalid-feedback"><?php echo $data['last_name_err'];?></span>
                </div>
                <div class="form-group">
                    <label for="password">Нова парола: <span> Ако не смятате да променяте паролата си, моля въведете текущата парола.</span></label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : '';?>">
                    <span class="invalid-feedback"><?php echo $data['password_err'];?></span>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Потвърди парола:</label>
                    <input type="password" name="confirm_password" class="form-control form-control-lg <?php echo (!empty($data['confirm_password_err'])) ? 'is-invalid' : '';?>">
                    <span class="invalid-feedback"><?php echo $data['confirm_password_err'];?></span>
                </div>
                <div class="row">
                    <div class="col-md-6 mx-auto">
                        <input type="submit" value="Обнови" class="btn btn-success btn-block">
                    </div>
                </div>
          </form>
        </div>
    </div>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
