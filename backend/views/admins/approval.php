<?php require BACKENDROOT . '/views/inc/header.php'; ?>
<div class="container">
  <div class="row mt-5 mb-5">
    <div class="col">
      <h2 class="text-center">Непотвърдени събития</h2>
    </div>

  </div>
  <?php foreach ($data['events'] as $event): ?>
    <div class="row">
      <div class="col-md-3">
        <p><?php echo $event->name; ?></p>
      </div>
      <div class="col-md-3">
        <p><?php echo $event->address; ?></p>
      </div>
      <div class="col-md-3">
        <p><?php echo $event->short_desc; ?></p>
      </div>
      <div class="col-md-3">
        <a class="btn btn-primary" href="<?php echo URLROOT; ?>/events/display&id=<?php echo $event->id; ?>">Виж събитието</a>
      </div>
    </div>
    <hr>
  <?php endforeach; ?>
</div>
<?php require BACKENDROOT . '/views/inc/footer.php'; ?>
